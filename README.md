# Snack App

App for customer's shopping

## Tools

- Install Nodejs V12
- Install npm
- Install yarn
- Install expo
- Install Adnroid Studio in order to use ADV Manager (Android Emulator)

### Commands for start this project

- git clone <repository_path>
- cd snack-app
- yarn start or expo start --android for start directly

### Git commands for pushing in this repository

- git pull                      -> fetch all new data to your local project
- git status                    -> show the differences between you local project and gitlab repository
- git add .                     -> add all new files in order to commit changes
- git commit -m "descption"     -> add a description in order to push it then
- git push origin <branch_name> -> push your changes to the repository