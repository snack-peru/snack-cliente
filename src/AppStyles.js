import { StyleSheet, Dimensions } from 'react-native';

// screen sizing
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    tab: {
        width: width/2 - 5
    },
    textTab: {
        color: 'white'
    }
});