import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
    tab: {
        width: width / 2 - 5
    },
    textTab: {
        color: 'white'
    }
});