import React, { useState, useEffect } from 'react';
import { Button, Body, Text, Container, Content, Header, Icon, Left, Title, Segment, Form, Item, Label, Input } from 'native-base';
import styles from './EmailLogin.style';

import Login from '../../components/Login/Login';
import Registry from '../../components/Registry/Registry';

export default function EmailLogin({ navigation }) {
    const [action, setAction] = useState(0);

    let content = <Login />

    if (action === 0) {
        // el usuario quiere iniciar sesión
        content = <Login navigation={navigation} />
    } else if (action === 1) {
        // el usuario quiere registrarse
        content = <Registry navigation={navigation} />
    }

    const changeAction = action => {
        // action = 1 -> login
        //action = 2 -> Register
        setAction(action);
    }

    return (
        <Container>
            <Header hasSegment>
                <Left>
                    <Button transparent>
                        <Icon name="arrow-back" onPress={() => {navigation.goBack()}} />
                    </Button>
                </Left>
                <Body>
                    <Title>Accede a Snack</Title>
                </Body>
            </Header>
            <Segment>
                <Button first style={styles.tab} onPress={changeAction.bind(this, 0)} >
                    <Text style={styles.textTab}>Iniciar Sesión</Text>
                </Button>
                <Button style={styles.tab} onPress={changeAction.bind(this, 1)} >
                    <Text style={styles.textTab}>Registrate</Text>
                </Button>
            </Segment>
            {content}
        </Container>
    );
}