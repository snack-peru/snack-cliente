import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Animated,
    Image,
    TouchableOpacity,
    Dimensions,
    Platform,
} from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import MapView, { Marker } from 'react-native-maps';
import * as Location from 'expo-location';
import axios from '../../axios';

import { LoadingIndicator } from '../../components/Shared/LoadingIndicator'
import Rating from '../../components/Shared/Rating'

const { width, height } = Dimensions.get("window");
const CARD_HEIGHT = 225;
const CARD_WIDTH = width * 0.8;
const SPACING_FOR_CARD_INSET = width * 0.1 - 10;

export default function App({ navigation }) {

    const [isLoading, setIsLoading] = useState(true);
    const [markers, setMarkers] = useState([]);
    var storeIdList = []
    const [latestLatitude, setLatestLatitude] = useState(null)
    const [latestLongitude, setLatestLongitude] = useState(null)

    const [latitude, setLatitud] = useState(0);
    const [longitude, setLongitude] = useState(0);

    const [address, setAddress] = useState('');
    const [street, setStreet] = useState('');
    const [searchText, setSearchText] = useState('');

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
            }

            let location = await Location.getCurrentPositionAsync({});
            setLatitud(location.coords.latitude);
            setLongitude(location.coords.longitude);
        })();
    });

    const onRegionChange = region => {
        console.log(region.latitude, region.longitude)
        //Actualizar las tiendas cercanas cuando te mueves mas de 1 km
        // Llamar getMarkers solo cuando las nuevas coordenadas difieran más de 1km con la últimas tomadas
        if (latestLatitude === null || distanceBetweenCoordinates(latestLatitude, latestLongitude, region.latitude, region.longitude) > 0.015060) {
            setLatestLatitude(region.latitude)
            setLatestLongitude(region.longitude)
            getMarkers(region.latitude, region.longitude)
        }
    }

    function distanceBetweenCoordinates(x1, y1, x2, y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))
    }

    function getMarkers(latitude, longitude) {
        console.log("storeIDList:")
        console.log(storeIdList)
        var request = {
            storeIdList: storeIdList
        }
        axios.post("/api/customer/" + latitude + "/" + longitude + "/stores/" + 0.5, request).then(res => {
            res.data.map(store => {
                console.log("res.data.map")
                storeIdList.push(store.storeId)
                
            })
            console.log(storeIdList)

            setMarkers(markers.concat(res.data))
        }).catch(e => {
            console.log(e)
        })
    }

    let mapIndex = 0;
    let mapAnimation = new Animated.Value(0);

    useEffect(() => {
        mapAnimation.addListener(({ value }) => {
            let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
            if (index >= markers.length) {
                index = markers.length - 1;
            }
            if (index <= 0) {
                index = 0;
            }

            clearTimeout(regionTimeout);

            const regionTimeout = setTimeout(() => {
                if (mapIndex !== index) {
                    mapIndex = index;
                    const { coordinate } = markers[index];
                    _map.current.animateToRegion(
                        {
                            ...coordinate,
                            latitudeDelta: 0.0022,
                            longitudeDelta: 0.0021
                        },
                        350
                    );
                }
            }, 10);
        });
    });

    const interpolations = markers.length ? markers.map((marker, index) => {
        const inputRange = [
            (index - 1) * CARD_WIDTH,
            index * CARD_WIDTH,
            ((index + 1) * CARD_WIDTH),
        ];

        const scale = mapAnimation.interpolate({
            inputRange,
            outputRange: [1, 1.5, 1],
            extrapolate: "clamp"
        });

        return { scale };
    }) : null;

    const onMarkerPress = (mapEventData) => {
        const markerID = mapEventData._targetInst.return.index;

        let x = (markerID * CARD_WIDTH) + (markerID * 20);
        if (Platform.OS === 'ios') {
            x = x - SPACING_FOR_CARD_INSET;
        }

        _scrollView.current.getNode().scrollTo({ x: x, y: 0, animated: true });
    }

    const _map = React.useRef(null);
    const _scrollView = React.useRef(null);

    let openCloseStyle;

    if (latitude) {
        return (
            <View style={{ flex: 1 }}>

                <MapView
                    ref={_map}
                    showsUserLocation
                    style={{ flex: 1 }}
                    initialRegion={{
                        latitude,
                        longitude,
                        latitudeDelta: 0.0022,
                        longitudeDelta: 0.0021
                    }}
                    onRegionChangeComplete={onRegionChange.bind(this)}
                >
                    {markers.length ? markers.map((marker, index) => {
                        const scaleStyle = {
                            transform: [
                                {
                                    scale: interpolations[index].scale
                                },
                            ],
                        };

                        return (
                            <Marker
                                key={index}
                                coordinate={marker.coordinate}
                                onPress={(e) => onMarkerPress(e)}
                            >
                                <Animated.View style={[styles.markerWrap]}>
                                    <Animated.Image
                                        source={require('../../../assets/marker-icon.png')}
                                        style={[styles.marker, scaleStyle]}
                                        resizeMode="cover"
                                    />
                                </Animated.View>
                            </Marker>
                        )
                    }) : null}

                </MapView>

                <View style={styles.searchBox}>
                    <TextInput
                        placeholder="Busca aquí..."
                        placeholderTextColor="#000"
                        autoCapitalize="none"
                        style={{ flex: 1, padding: 0 }}
                    />
                    <Ionicons name="ios-search" size={20} onPress={() => navigation.navigate('SearchStore')} />
                </View>

                <Animated.ScrollView
                    ref={_scrollView}
                    horizontal
                    pagingEnabled
                    scrollEventThrottle={1}
                    showsHorizontalScrollIndicator={false}
                    style={styles.scrollView}
                    snapToInterval={CARD_WIDTH + 20}
                    snapToAlignment="center"
                    contentInset={{ // Only ios
                        top: 0,
                        left: SPACING_FOR_CARD_INSET,
                        bottom: 0,
                        right: SPACING_FOR_CARD_INSET
                    }}
                    contentContainerStyle={{ // only android
                        paddingHorizontal: Platform.OS === 'android' ? SPACING_FOR_CARD_INSET : 0
                    }}
                    onScroll={Animated.event(
                        [
                            {
                                nativeEvent: {
                                    contentOffset: {
                                        x: mapAnimation,
                                    }
                                },
                            },
                        ],
                        { useNativeDriver: true }
                    )}
                >
                    {markers.length ? markers.map((marker, index) => {
                        if (marker.status === "Cerrado") {
                            openCloseStyle = {
                                backgroundColor: 'red'
                            }
                        } else {
                            openCloseStyle = {
                                backgroundColor: 'rgba(0, 128, 0,  1)'
                            }
                        }
                        return (
                            <View style={styles.card} key={index}>
                                <View style={styles.bannerStore} >
                                    <Image
                                        source={{ uri: marker.image }}
                                        style={styles.cardImage}
                                        resizeMode="cover"
                                    />
                                    <View style={styles.openCloseTag} >
                                        <Text style={[styles.openCloseText, openCloseStyle]}>{marker.status}</Text>
                                    </View>
                                </View>

                                <View style={styles.textContent}>
                                    <Text numberOfLines={1} style={styles.cardtitle}>{marker.title}</Text>
                                    <Rating rating={marker.rating} reviews={marker.reviews} />
                                    <Text numberOfLines={1} style={styles.cardDescription}>{marker.description}</Text>
                                    <View style={styles.button}>
                                        <TouchableOpacity
                                            onPress={() => { }}
                                            style={[styles.borderShowStoreButton, {
                                                borderColor: '#FF6347',
                                                borderWidth: 1
                                            }]}
                                        >
                                            <Text style={[styles.textShowStoreButton, {
                                                color: '#FF6347'
                                            }]} onPress={() => { }} >Ver tienda</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        )

                    }) : null}
                </Animated.ScrollView>

            </View>

        );
    }
    return (
        <LoadingIndicator />
    );


}

const styles = StyleSheet.create({
    searchBox: {
        position: 'absolute',
        marginTop: Platform.OS === 'ios' ? 40 : 20,
        flexDirection: "row",
        backgroundColor: '#fff',
        width: '90%',
        alignSelf: 'center',
        borderRadius: 5,
        padding: 10,
        shadowColor: '#ccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
        width: 50,
        height: 50,
    },
    marker: {
        width: 30,
        height: 30,
    },
    card: {
        elevation: 2,
        backgroundColor: "#FFF",
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
    },
    cardImage: {
        flex: 2.5,
        width: "100%",
        height: "100%",
        alignSelf: "center",
        position: 'absolute'
    },
    textContent: {
        flex: 2,
        padding: 10,
    },
    cardtitle: {
        fontSize: 12,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
    },
    button: {
        alignItems: 'center',
        marginTop: 5
    },
    borderShowStoreButton: {
        width: '100%',
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3
    },
    textShowStoreButton: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    scrollView: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 10,
    },
    numberOfValorations: {
        fontSize: 12,
        color: '#444'
    },
    openCloseTag: {
        height: 'auto',
        backgroundColor: 'rgba(0, 128, 0,  1)',
        width: '20%',
        borderRadius: 5,
        alignSelf: 'flex-end'
    },
    openCloseText: {
        textAlign: 'center',
        color: 'white',
        fontWeight: "bold"
    },
    bannerStore: {
        flex: 2.5,
        width: "100%",
        height: "100%",
        alignSelf: "center"
    }
});
