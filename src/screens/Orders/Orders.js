import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Container } from 'native-base';
import styles from './Orders.styles';

import HeaderTemplate from '../../navigations/Headers/Hamburger';

export default function Orders({ navigation }) {
  return (
    <Container>
      <HeaderTemplate title='Snack' subtitle='' navigation={navigation} />

      <View style={styles.container}>
          <Text>Pedidos</Text>
        </View>
    </Container>
  );
}