import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    textStatusStoreOpen: {
        borderWidth: 1, 
        borderRadius: 10,
        color: 'blue',
        borderColor: 'blue',
        fontSize: 13,
        top: 1,
        paddingLeft: '6%',
        paddingRight: '5%',
        paddingTop: '0.6%'
    },
    textStatusStoreClosed: {
        borderWidth: 1, 
        borderRadius: 10,
        color: 'red',
        borderColor: 'red',
        fontSize: 13,
        top: 1,
        paddingLeft: '6%',
        paddingRight: '5%',
        paddingTop: '0.6%'
    },
    textButtonVerUbicacion: {
        borderWidth: 1, 
        borderRadius: 10,
        color: 'black',
        borderColor: 'black',
        fontSize: 15,
        top: 1,
        paddingLeft: '18%',
        paddingRight: '18%',
        paddingTop: '3%',
        paddingBottom: '3%'
    },
    textButtonVerComentarios: {
        borderWidth: 1, 
        borderRadius: 10,
        color: 'black',
        borderColor: 'black',
        fontSize: 15,
        top: 1,
        paddingLeft: '14%',
        paddingRight: '14%',
        paddingTop: '3%',
        paddingBottom: '3%'
    },
    viewHeader:{
        flexDirection: 'row',
        width:'100%'
    },
    viewIconBack:{
        width:'12%'
    },
    iconBack:{
        color:'white',
        top:'25%'
    },
    viewSpaceBetweenStoreNameAndIconBack:{
        width:'4%'
    },
    viewStoreName:{
        width:'65%',
        justifyContent: 'center', 
        alignItems: 'center'
    },
    textStoreName:{
        fontSize:20,
        color:'white',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    viewHeartSearchProducts:{
        width:'21%'
    },
    iconHeart:{
        color:'white',top:'32%',left:'10%'
    },
    iconSearchProducts:{
        color:'white',left:'65%',bottom:'13%'
    },
    viewDetailsStore:{
        borderWidth:1
    },
    viewFirstRowDetailsStore:{
        flexDirection: 'row'
    },
    viewScoreStore:{
        flexDirection: 'row',width: '30%',justifyContent: 'center', alignItems: 'center'
    },
    textScoreStore:{
        fontSize: 16
    },
    iconScoreStore:{
        left:'2%',bottom:'0.5%'
    },
    viewDistanceStore:{
        flexDirection: 'row',width: '40%',justifyContent: 'center', alignItems: 'center'
    },
    textMetersDistanceStore:{
        fontSize: 16
    },
    textDistanceStore:{
        fontSize: 14
    },
    viewStatusStore:{
        flexDirection: 'row',width: '30%',justifyContent: 'center', alignItems: 'center'
    },
    viewSecondRowDetailsStore:{
        flexDirection: 'row'
    },
    viewDeliveryTypeStore:{
        flexDirection: 'row',width: '30%',justifyContent: 'center', alignItems: 'center'
    },
    textDeliveryTypeStore:{
        fontSize: 14
    },
    viewDeliveryPriceStore:{
        flexDirection: 'row',width: '40%',justifyContent: 'center', alignItems: 'center'
    },
    iconDeliveryPriceStore:{
        color:'gray'
    },
    textDeliveryPriceStore:{
        fontSize: 14,width: '45%',left:'30%',color:'gray'
    },
    viewTimeOfAttention:{
        flexDirection: 'row',width: '30%',justifyContent: 'center', alignItems: 'center'
    },
    textTimeOfAttention:{
        fontSize: 15
    },
    viewImageStore:{
        width: '100%', justifyContent: 'center', alignItems: 'center'
    },
    viewMapAndComments:{
        width: '100%',flexDirection:'row',borderWidth:1
    },
    viewButtonMapAndComments:{
        width:'50%',flexDirection:'row',justifyContent: 'center', alignItems: 'center',marginTop:'0%',marginBottom: '5%'
    },
    viewProductos:{
        width: '100%',flexDirection:'row'
    },
    textProductos:{
        fontSize:24,fontWeight:'bold',left:'20%'
    }

});