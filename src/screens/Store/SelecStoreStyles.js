import { StyleSheet, Dimensions } from 'react-native';

const screen_height= Dimensions.get('window').height;

export default StyleSheet.create({
    labelContainer: {
      paddingLeft: '5%',
      paddingTop: '5%',
      paddingBottom: '5%'
    },
    label:  {
      fontSize: 15
    },
    switchContainer: {
      paddingLeft: '5%',
      paddingRight: '5%',
      paddingBottom: '5%'
    },
    noDataContainer: {
      alignItems: 'center',
      justifyContent: 'center',
      marginLeft: '5%',
      marginRight: '5%',      
      height: screen_height/1.5,
      borderRadius: 10,
    }, 
    listContainer: {
      marginLeft: '5%',
      marginRight: '5%',      
      height: screen_height/1.5,
      borderRadius: 10,
    },
});