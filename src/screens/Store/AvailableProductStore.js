import React, { Fragment } from 'react';
import { View, StyleSheet, Button as ButtonBase } from 'react-native';
import { Container, Header, Body, Button, Title, Subtitle, Icon, Content, List, Text } from 'native-base';
import ProductListWithQuantity from '../../components/ProductList/ProductListWithQuantity';

export default function AvailableProductStore({ route, navigation }) {

    const {
        idStore,
        nameStore,
        availableProducts,
        deliveryType,
        valoration,
        missingProducts
    } = route.params;

    function getTotalPrice() {
        let total = 0;
        availableProducts.forEach(element => {
            total += element.price * element.quantity;
        });
        return total;
    }

    let MissingText = "";
    let ColorMissingText = "";
    if (missingProducts.length > 0) {
        if (missingProducts.length === 1)
            MissingText = "Falta " + missingProducts.length + " producto";
        else
            MissingText = "Faltan " + missingProducts.length + " productos";
        ColorMissingText = 'yellow';
    } else {
        MissingText = "Completo";
        ColorMissingText = '#8cff96';
    }

    return (
        <Container>
            <Header>
                <Button transparent>
                    <Icon name="arrow-back" onPress={() => { navigation.goBack() }} />
                </Button>
                <Body>
                    <Title>{nameStore}</Title>
                    <Subtitle style={{ color: ColorMissingText }}>{MissingText}</Subtitle>
                </Body>
            </Header>
            <Content>
                <List>
                    <View>
                        {
                            availableProducts.length > 0 ?
                                (
                                    <View style={styles.availabilityContainer}><Text style={styles.availabilityText}>Disponibles</Text></View>) :
                                (
                                    <View style={styles.availabilityContainer}><Text style={styles.nonProductsAvailablesText}>No hay productos disponibles</Text></View>
                                )}
                        {availableProducts.map((product, index) => {
                            return (
                                <ProductListWithQuantity
                                    key={index}
                                    idProduct={product.productId}
                                    brandName={product.brand}
                                    productName={product.name}
                                    unitOfMeasurement={product.measurement}
                                    quantity={product.quantity}
                                    unitPrice={product.price}
                                    totalPrice={product.price * product.quantity}
                                    isShowPrices={true}
                                />
                            );
                        })
                        }
                    </View>
                    <View>
                        {
                            missingProducts.length > 0 ?
                                (
                                    <View style={styles.availabilityContainer}><Text style={styles.availabilityText}>Faltantes</Text></View>
                                ) :
                                (
                                    <View></View>
                                )
                        }
                        {missingProducts.map((product, index) => {
                            return (
                                <ProductListWithQuantity
                                    key={index}
                                    idProduct={product.productId}
                                    brandName={product.brand}
                                    productName={product.name}
                                    unitOfMeasurement={product.measurement}
                                    quantity={product.quantity}
                                    unitPrice={product.price}
                                    totalPrice={product.price * product.quantity}
                                    isShowPrices={false}
                                />
                            );
                        })
                        }
                    </View>
                </List>
            </Content>
            <View>
                <Fragment>
                    <View style={styles.subtotalContainer}>
                        <View style={styles.subtotalView}>
                            <Text>Subtotal: </Text>
                            <Text style={styles.subtotalPrice}>{"S/." + getTotalPrice().toFixed(2)}</Text>
                        </View>
                        <View style={styles.subtotalView}>
                            <ButtonBase title="Elegir tienda" onPress={() => console.log("Elegir tienda")} ></ButtonBase>
                        </View>
                    </View>
                </Fragment>
            </View>
        </Container >

    );
}

const styles = StyleSheet.create({
    subtotalContainer: {
        flexDirection: 'row',
        padding: 10,
        paddingLeft: 10,
        borderColor: 'gray',
        borderWidth: 0.45,
        justifyContent: 'space-between'
    },
    subtotalView: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    subtotalPrice: {
        fontSize: 19,
        fontWeight: 'bold'
    },
    availabilityContainer: {
        paddingTop: 18,
        paddingLeft: 18 
    },
    availabilityText: {
        fontSize: 22,
    },
    nonProductsAvailablesText: {
        fontSize: 15,
        textAlign: 'center',
    }
});