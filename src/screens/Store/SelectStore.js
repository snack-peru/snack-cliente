import React, {useState, useEffect} from 'react';
import { ScrollView, Text, View } from 'react-native';
import { Container } from 'native-base';
import styles from './SelecStoreStyles';
import HeaderWithBackArrowAndIcon from '../../components/Shared/HeaderWithBackArrowAndIcon';
import SwitchSelector from 'react-native-switch-selector';
import { LoadingIndicator } from '../../components/Shared/LoadingIndicator';
import AsyncStorage from '@react-native-community/async-storage';
import { useFocusEffect } from '@react-navigation/native';
import { StoreListItem } from '../../components/Store/StoreListItem';
import axios from '../../axios';

import { logger } from 'react-native-logs';
var log = logger.createLogger();

export default function SelectStore( { route, navigation} ) {
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingStores, setIsLoadingStores] = useState(false);

  const [selectedDeliveryType, setSelectedDeliveryType] = useState('onStore');
  const [storeList, setStoreList] = useState([]);
  const [deliveryTypes, setDeliveryTypes] = useState([
    { label: 'En tienda', value: 'onStore' },
    { label: 'Delivery', value: 'delivery' }
  ]);

  const [page, setPage] = useState(1);
  const [pageLen, setPageLen] = useState(10);
  const { productList } = route.params;

  const fetchData = async () => {
    setIsLoadingStores(true);
    const idCustomer = await AsyncStorage.getItem('idCustomer');
    if (idCustomer) {
      try {
        
        let products = productList.map((p) => {
          return {
              id: p.productId,
              quantity: p.quantity
          } 
        })
        const body = {
          idCustomer,
          products,
          deliveryType: selectedDeliveryType,
          distance: 1,
          longitude: -77.005826,
          latitude: -12.058483,
          page,
          pageLen
        };
        log.debug("FETCH DATA ON: /api/customer/basket/listStores");
        //log.debug("BODY: ", body);
        const response = await axios.post("/api/customer/basket/listStores", body);
        setStoreList(response.data);
        
        log.debug("RESPONSE: ", response.data);
      } catch (e) {
        log.error("ERROR ON FETCH DATA: ", e);
      }
    }
    setIsLoadingStores(false);
  };

  useFocusEffect(
    React.useCallback(() => {
      fetchData(); 
    }, [])
  );

  useEffect(() => {
    fetchData()
  }, [selectedDeliveryType]);

  const updateDeliveryType = async (deliveryType) => {
    log.info("Deliver type selected: ", deliveryType);
    setSelectedDeliveryType(deliveryType);
  }

  const onSelectStore = (value) => {
    log.info("Store selected: ", value);
  }

  const getAvailableProducts = (store) => {
    let listOfAvailableProducts = [];
    store.products.forEach(element => {
      let product = route.params.productList.find((elem) => elem.productId === element.idProduct);
      product.price = element.price;
      listOfAvailableProducts.push(product);
    });
    return listOfAvailableProducts;
  }

  const getMissingProducts = (store) => {
    let listOfBasketProducts = route.params.productList;
    let listOfAvailableProducts = getAvailableProducts(store);
    var listMissingProducts = listOfBasketProducts.filter(x => listOfAvailableProducts.indexOf(x) === -1);
    return listMissingProducts;
  }

  const getSubtotal = (store) => {
    let subtotal = 0;
    store.products.forEach(element => {
      subtotal += element.price * route.params.productList.find((elem) => elem.productId === element.idProduct).quantity;
    });
    return subtotal;
  };

  return (
    <Container>
      <HeaderWithBackArrowAndIcon
        title="Elige tu bodega"
        iconFlag={false}
        iconName="search"
        navigation = {navigation}
        handleAction={()=>{}}
      /> 

      <View style={styles.labelContainer}>
        <Text style={styles.label}>Escoge tu tipo de entrega</Text>
      </View>

      <View style={styles.switchContainer}>
        <SwitchSelector
          options={deliveryTypes}
          initial={0}
          onPress={updateDeliveryType}
          buttonColor="#2F329F"
          hasPadding={true}
        />
      </View>

      {isLoadingStores === true? (
        <View style={styles.noDataContainer}>
          <LoadingIndicator/>
        </View>          
      ): (          
        storeList.length === 0? (
          <View style={styles.noDataContainer}>
            <Text>Lo sentimos, no hay tiendas disponibles</Text>
          </View>
        ): (
          <ScrollView style={styles.listContainer}>
            {storeList.map((item, index) => (
              <StoreListItem
                key= {index}
                selectHandler = {onSelectStore}
                idStore = {item.idStore}
                availableProducts = {getAvailableProducts(item)}
                deliveryType = {deliveryTypes}
                name = {item.name}
                missingProducts = {getMissingProducts(item)}
                valoration = {item.score}
                currency = {"S/"}
                price = {getSubtotal(item)}
                navigation={navigation}
              />
            ))}
          </ScrollView>
        )
      )}

    </Container>
  );
  
}