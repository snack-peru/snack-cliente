
import React, { useState} from 'react';
import { Text, View, Dimensions } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import axios from '../../axios';
import { Header,Container,Icon,Thumbnail} from 'native-base';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './StoreProfile.styles';
import {ScrollView } from 'react-native-gesture-handler';
import StoreProfileCategories from '../../components/Store/StoreProfileCategories';

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

function StoreProfile({route, navigation}) {
    const [listOfProducts, setListOfProducts] = useState([]);
    console.log(route.params)

    function getProfileProducts() {
        axios.get("/api/customer/storexproduct/getProfileProducts/" + route.params.idStore).then(res => {
            res.data.map((item) => {
                    console.log(item);
            });
            setListOfProducts(res.data);

        }).catch(error => {
            console.log(error,"Que fue");
        })
    }

    useFocusEffect(
        React.useCallback(() => {
          getProfileProducts();
        }, [])
      )

  return (
    <Container>
        <Header>
            <View style={styles.viewHeader}>
                <View style={styles.viewIconBack}>
                    <Icon style={styles.iconBack} name="arrow-back" onPress={() => { navigation.goBack() }} />
                </View>
                <View style={styles.viewSpaceBetweenStoreNameAndIconBack}>
                </View>
                <View style={styles.viewStoreName}>
                    <Text style={styles.textStoreName}>{route.params.name}</Text>
                </View>
                <View style={styles.viewHeartSearchProducts}>
                    <AntDesign name="hearto" size={22} style={styles.iconHeart}/>
                    <Icon name="ios-search" style={styles.iconSearchProducts} />
                </View>
            </View>
        </Header>
        <ScrollView>
            <View style={styles.viewDetailsStore}>
            <View style={styles.viewFirstRowDetailsStore}>
                <View style = {styles.viewScoreStore}>
                    <Text style={styles.textScoreStore}>{route.params.score}</Text>
                    <MaterialCommunityIcons name="star-outline" size={22} style={styles.iconScoreStore} />
                </View>
                <View style = {styles.viewDistanceStore}>
                    <Text style={styles.textMetersDistanceStore}>50m</Text>
                    <Text style={styles.textDistanceStore}> de distancia</Text>
                </View>
                <View style = {styles.viewStatusStore}>
                    {route.params.statusStore.name==='Abierto'
                        ?<Text style={styles.textStatusStoreOpen}>{route.params.statusStore.name}</Text>
                        :<Text style={styles.textStatusStoreClosed}>{route.params.statusStore.name}</Text>
                    } 
                </View>
            </View>
            <View style={styles.viewSecondRowDetailsStore}>
                <View style = {styles.viewDeliveryTypeStore}>
                    <Text style={styles.textDeliveryTypeStore}>{route.params.deliveryType.name}</Text>
                </View>
                <View style = {styles.viewDeliveryPriceStore}>
                    <MaterialCommunityIcons name="bike" size={20} style={styles.iconDeliveryPriceStore} />
                    <Text style={styles.textDeliveryPriceStore}>S/. 2.50</Text>
                </View>
                <View style = {styles.viewTimeOfAttention}>
                    {(route.params.opentime!==null & route.params.closedTime!==null) 
                        ?<Text style={styles.textTimeOfAttention}>{route.params.openTime} - {route.params.closedTime}</Text>
                        :<Text style={styles.textTimeOfAttention}>--</Text>
                    } 
                </View>
            </View>
            </View>
            <View style={styles.viewImageStore}>
                <Thumbnail square large source={{ uri: route.params.image }}style={{ width : deviceWidth * 90 / 100, height: deviceHeight * 30/100 , resizeMode: "contain"}} />
            </View>
            <View style={styles.viewMapAndComments}>
                <View style={styles.viewButtonMapAndComments}>
                    <Text style={styles.textButtonVerUbicacion}>Ver ubicacion</Text>
                </View>
                <View style={styles.viewButtonMapAndComments}>
                    <Text style={styles.textButtonVerComentarios}>Ver Comentarios</Text>
                </View>
            </View>
            <View style={styles.viewProductos}>
                <Text style={styles.textProductos}>Productos</Text>
            </View>
            
            {listOfProducts.map((category, index) => 
                {
                    return(
                        <StoreProfileCategories key={index} categories={category}/>
                    );
                })
            }

        </ScrollView>

    </Container>
  );

}

export default StoreProfile