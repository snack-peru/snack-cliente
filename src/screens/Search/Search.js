import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { TouchableNativeFeedback, ScrollView } from 'react-native-gesture-handler';
import { LoadingIndicator } from '../../components/Shared/LoadingIndicator';
import { Container, Header, Item, Input, Icon, Button, Text, Body, Left, Right } from 'native-base';

import axios from '../../axios';
import ProductList from '../../components/ProductList/ProductList';

export default function Search({ navigation }) {

    const [isLoading, setIsLoading] = useState(false);

    const [searchText, setSearchText] = useState('');
    const [products, setProducts] = useState([]);
    const [mapPrices, setMapPrices] = useState(new Map());
    let mapTmp = new Map();
    let lsProductsId = [];
    let showList = '';
    function searchProducts() {
        setIsLoading(true);
        axios.get("/api/customer/product/search/" + searchText).then(res => {
            res.data.map((item) => {
                if (item.image !== null) {
                    let imageProductUrl = axios.defaults.baseURL + "/api/customer/product/image/" + item.image;
                    item.image = imageProductUrl;
                } else {
                    item.image = "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png";
                }

                lsProductsId.push({ productId: item.idProduct });
                return item;
            });

            axios.post("/api/customer/ListAproxPrice/", lsProductsId).then(res => {
                res.data.map((item) => {
                    mapTmp.set(item.productId, item.aproxPrice);
                })
                setMapPrices(mapTmp);
            });

            setProducts(res.data);
            setIsLoading(false);
        }).catch(error => {
            console.log(error);
        })
    }

    if (isLoading) {
        showList = <Container>
            <LoadingIndicator />
        </Container>

    } else {
        showList = <View>
            {products.map((product, index) => {
                let pricetmp;
                if (mapPrices.get(product.idProduct)) {
                    pricetmp = mapPrices.get(product.idProduct);
                } else {
                    pricetmp = undefined;
                }
                return (
                    <TouchableNativeFeedback key={index} onPress={() => navigation.navigate('ProductDetail', { productId: product.idProduct })}>
                        <ProductList product={product} price={pricetmp} />
                    </TouchableNativeFeedback>
                );
            })}
        </View>
    }

    return (
        <Container>
            <Header searchBar rounded>
                <Left style={{
                flex: 0.2,
            }}>
                    <Button transparent>
                        <Icon name="arrow-back" onPress={() => { navigation.goBack() }} />
                    </Button>
                </Left>
                <Item style={{ backgroundColor: 'white', width: '50%',}}>
                    <Icon name="ios-search" />
                    <Input style={{ width: '100%' }} placeholder="Buscar aquí ..." onChangeText={(x) => { setSearchText(x); }} value={searchText} />
                </Item>
                <Right style={{
                flex: 0.5,
            }}>
                    <Button style={{ padding: 0, margin: 0, width: 90, height: 40, backgroundColor: '#2185ff' }} onPress={() => searchProducts()}>
                        <Text>Search</Text>
                    </Button>
                </Right>
            </Header>
            <ScrollView style={{ padding: 5 }}>
                {showList}
            </ScrollView>
        </Container>
    );
}
