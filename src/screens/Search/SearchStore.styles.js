import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    textStatusStoreOpen: {
        borderWidth: 1,
        borderRadius: 10,
        color: 'blue',
        borderColor: 'blue',
        fontSize: 13,
        top: 1,
        paddingLeft: '6%',
        paddingRight: '5%',
        paddingTop: '0.6%'
    },
    textStatusStoreClosed: {
        borderWidth: 1,
        borderRadius: 10,
        color: 'red',
        borderColor: 'red',
        fontSize: 13,
        top: 1,
        paddingLeft: '6%',
        paddingRight: '5%',
        paddingTop: '0.6%'
    },
    viewSeeStoreInMap: {
        borderWidth: 1,
        borderRadius: 10,
        color: '#6495ED',
        borderColor: '#6495ED',
        fontSize: 13,
        top: 1,
        paddingLeft: '6%',
        paddingRight: '5%',
        paddingTop: '0.6%',
        flexDirection: 'row',
        top: '-19%'
    },
    viewStoreSearched: {
        flexDirection: 'row',
        padding: 20
    },
    viewImageStoreSearched: {
        width: '28%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageStoreSearched: {
        width: 88,
        height: 90,
        resizeMode: "contain"
    },
    viewDetailsStoreSearched: {
        width: '72%',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column'
    },
    textStoreName: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    viewScoreDistanceStatusStore: {
        flexDirection: 'row'
    },
    viewScoreStore: {
        flexDirection: 'row',
        width: '25%'
    },
    textScoreStore: {
        fontSize: 16
    },
    iconScoreStore: {
        left: '2%',
        bottom: '0.5%'
    },
    textDistanceStore: {
        fontSize: 16,
        width: '50%'
    },
    textDeliveryTypeStore: {
        fontSize: 14,
        width: '50%'
    },
    viewDeliveryPriceMapStore: {
        flexDirection: 'row'
    },
    iconDeliveryTypeStore: {
        color: 'gray'
    },
    textDeliveryPriceStore: {
        fontSize: 14,
        width: '45%',
        left: '30%',
        color: 'gray'
    },
    iconButtonMapStore: {
        color: '#6495ED',
        right: '10%'
    },
    textButtonMapStore: {
        fontSize: 14,
        color: '#6495ED'
    },
    buttonBack: {
        width: '13%',
        right: '4%'
    },
    itemSearchHeader: {
        backgroundColor: 'white',
        right: '4%'
    },
    iconSearchHeader: {
        right: '10%'
    },
    inputSearchHeader: {
        right: '22%'
    },
    buttonSearchStoreHeader: {
        padding: 0,
        margin: 0,
        width: 70,
        height: 35,
        backgroundColor: '#2185ff'
    },
    textButtonSearchStoreHeader: {
        textAlign: 'center',
        fontSize: 12
    },
    paddingScrollView: {
        padding: 5
    }

});