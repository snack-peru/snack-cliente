import React, { useState} from 'react';
import { View } from 'react-native';
import { TouchableNativeFeedback, ScrollView } from 'react-native-gesture-handler';
import { LoadingIndicator } from '../../components/Shared/LoadingIndicator';
import { Container, Header, Item, Input, Icon, Button, Text,Thumbnail} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './SearchStore.styles';


import axios from '../../axios';

export default function SearchStore({ navigation }) {

    const [isLoading, setIsLoading] = useState(false);

    const [searchText, setSearchText] = useState('');
    const [stores, setStores] = useState([]);
    let showList = '';
    function searchStores() {
        setIsLoading(true);
        axios.get("/api/customer/store/search/" + searchText).then(res => {
            res.data.map((item) => {
                    if(item.image===null){
                        item.image = "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png";
                        return item;
                    }
                    else{
                        return item;
                    }
            });

            setStores(res.data);
            setIsLoading(false);
        }).catch(error => {
            console.log(error);
        })
    }

    if (isLoading) {
        showList = <Container>
            <LoadingIndicator />
        </Container>

    } else {
        showList = <View>
            {stores.map((store, index) => {
                return (
                    <TouchableNativeFeedback key={index} onPress={() => {navigation.navigate('StoreProfile', store)}}>
                        <View style={styles.viewStoreSearched}>
                            <View style={styles.viewImageStoreSearched}>
                                <Thumbnail square large source={{ uri: store.image }}style={styles.imageStoreSearched} />
                            </View>
                            <View style={styles.viewDetailsStoreSearched}>
                                <Text style={styles.textStoreName}>{store.name}</Text>
                                <View style = {styles.viewScoreDistanceStatusStore}>
                                    <View style = {styles.viewScoreStore}>
                                        <Text style={styles.textScoreStore}>{store.score}</Text>
                                        <MaterialCommunityIcons name="star-outline" size={22} style={styles.iconScoreStore} />
                                    </View>
                                    <Text style={styles.textDistanceStore}>130 metros</Text>
                                    {store.statusStore.name==='Abierto'
                                     ?<Text style={styles.textStatusStoreOpen}>{store.statusStore.name}</Text>
                                     :<Text style={styles.textStatusStoreClosed}>{store.statusStore.name}</Text>
                                    }    
                                </View>
                                    <Text style={styles.textDeliveryTypeStore}>{store.deliveryType.name}</Text>
                                <View style = {styles.viewDeliveryPriceMapStore}>
                                    <MaterialCommunityIcons name="bike" size={20} style={styles.iconDeliveryTypeStore} />
                                    <Text style={styles.textDeliveryPriceStore}>S/. 2.50</Text>
                                    <View style={styles.viewSeeStoreInMap}>
                                        <MaterialCommunityIcons name="map-marker" size={20} style={styles.iconButtonMapStore} />
                                        <Text style={styles.textButtonMapStore} >Ver en mapa </Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableNativeFeedback>
                );
            })}
        </View>
    }

    return (
        <Container>
            <Header searchBar rounded>
                <Button transparent style ={styles.buttonBack}>
                    <Icon name="arrow-back" onPress={() => { navigation.goBack() }} />
                </Button>
                <Item style={styles.itemSearchHeader}>
                    <Icon name="ios-search" style={styles.iconSearchHeader} />
                    <Input style={styles.inputSearchHeader} placeholder="Escribe tu tienda aqui..." onChangeText={(x) => { setSearchText(x); }} value={searchText} />
                </Item>
                <Button style={styles.buttonSearchStoreHeader} onPress={() => searchStores()}>
                    <Text style={styles.textButtonSearchStoreHeader}>Buscar</Text>
                </Button>
            </Header>
            <ScrollView style={styles.paddingScrollView}>
                {showList}
            </ScrollView>
        </Container>
    );
}
