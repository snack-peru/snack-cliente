import React, { useState, useEffect } from 'react';
import { Text } from 'react-native';
import { Container, Content, Body, ListItem, Input, Item, Icon, Left, Right, Radio, Button } from 'native-base';
import styles from './Address.styles';

import HeaderTemplate from '../../navigations/Headers/Hamburger';
import AsyncStorage from '@react-native-community/async-storage';
import axios from '../../axios';

import ConfirmationModal from '../../components/Modals/ConfirmationModal'

export default function Address({ navigation }) {

    const [addressList, setAddressList] = useState([])
    const [customerId, setCustomerId] = useState()
    const [visible, setVisible] = useState(false);
    const [idAddressToDelete, setIdAddressToDelete] = useState()

    function toggleOverlay(idAddress) {
        
        if (idAddress !== null) {
            // idAddressToDelete = idAddress
            setIdAddressToDelete(idAddress)
        } else {
            setIdAddressToDelete(null)
        }
        setVisible(!visible);
    }

    function confirmOverlay() {
        // llamar al API que elimina la dirección
        axios.delete("/api/customer/" + customerId + "/address/" + idAddressToDelete).then(res => {
            // Eliminar el idAddress del array
            axios.get("/api/customer/" + customerId + "/address").then(res => {
                setAddressList(res.data)
            }).catch(e => {
                console.log(e)
            })
        }).catch(e => {
            console.log(e)
        })
    }

    async function getAllAddressForCustomer() {
        var idCustomer = await AsyncStorage.getItem('idCustomer');
        setCustomerId(idCustomer)

        axios.get("/api/customer/" + idCustomer + "/address").then(res => {
            setAddressList(res.data)
        }).catch(e => {
            console.log(e)
        })
    }

    useEffect(() => {
        getAllAddressForCustomer()

    }, []);

    function searchSubmit() {
        navigation.navigate('NewAddress')
    }

    function changeSelection(idAddress, isDefault) {
        if (!isDefault) {
            let arr = addressList

            axios.put("/api/customer/" + customerId + "/address/" + idAddress).then(res => {
                let longResponse = res.data.id
                let arrResponse = arr.map(element => {
                    if (element.idAddress === longResponse) {
                        element.isDefault = false
                        return element
                    } else if (element.idAddress === idAddress) {
                        element.isDefault = true
                        return element
                    } else {
                        return element
                    }
                })
                setAddressList(arrResponse)

            }).catch(e => {
                console.log(e)
            })
        }

    }

    return (
        <Container>
            <HeaderTemplate secondaryHeader="gobackOnly" title="Cambia tu dirección" navigation={navigation} />

            <Content style={styles.content}>
                <Item regular>
                    <Input
                        placeholder='Ingresa una dirección'
                        keyboardType="default"
                        onSubmitEditing={searchSubmit.bind(this)}
                    />
                    <Icon active name='search' />
                </Item>

                <ListItem icon onPress={searchSubmit.bind(this)}>
                    <Left>
                        <Button>
                            <Icon active name="pin" />
                        </Button>
                    </Left>
                    <Body>
                        <Text>Usar ubicación actual</Text>
                    </Body>
                    <Right />
                </ListItem>

                {addressList.map((address, index) => {
                    return (
                        <ListItem icon key={index} onLongPress={toggleOverlay.bind(this, address.idAddress)} >
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="pin" />
                                </Button>
                            </Left>
                            <Body>
                                <Text>{address.description}</Text>
                            </Body>
                            <Right>
                                <Radio selected={address.isDefault} onPress={changeSelection.bind(this, address.idAddress, address.isDefault)} />
                            </Right>
                        </ListItem>

                    )
                })}

                <ConfirmationModal
                    visible={visible}
                    toggleOverlay={toggleOverlay}
                    question={'¿Está seguro que desea eliminar la dirección?'}
                    cancelAction={toggleOverlay}
                    confirmAction={confirmOverlay}
                    value={idAddressToDelete}
                />

            </Content>
        </Container>
    );
}