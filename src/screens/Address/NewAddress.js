import React, { useState, useEffect } from 'react';
import { Image, Text, View, Button } from 'react-native';
import { Container, Input } from 'native-base';
import styles from './Address.styles';

import MapView from 'react-native-maps';
import * as Location from 'expo-location';

import marker from '../../../assets/marker-icon.png'

import AsyncStorage from '@react-native-community/async-storage';
import axios from '../../axios';

import HeaderTemplate from '../../navigations/Headers/Hamburger';

export default function NewAddress({ navigation }) {

    const [latitude, setLaitud] = useState(0);
    const [longitude, setLongitude] = useState(0);

    let address

    const [inputText, setInputText] = useState('')

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
            }

            let location = await Location.getCurrentPositionAsync({});
            setLaitud(location.coords.latitude);
            setLongitude(location.coords.longitude);
        })();
    });

    async function getAddress(latitude, longitude) {
        address = await Location.reverseGeocodeAsync({ latitude, longitude });
    }

    async function onRegionChange(region) {
        setLaitud(region.latitude);
        setLongitude(region.longitude);
        await getAddress(region.latitude, region.longitude);
        await changeAddressName(address)

        console.log(region.latitude, region.longitude)
    }

    async function changeAddressName(addressInput) {
        if (typeof addressInput[0].street != "undefined" && typeof addressInput[0].name != "undefined") {
            setInputText(addressInput[0].street + " " + addressInput[0].name)
        }
    }

    function onChangeAddress(text) {
        setInputText(text)
    }

    async function addAddress() {
        let idCustomer = await AsyncStorage.getItem('idCustomer');
        var addressRequest = {
            "description": inputText,
            "isDefault": true,
            "latitude": latitude,
            "longitude": longitude,
            "status": 1
        }
        axios.post("/api/customer/" + idCustomer + "/address", addressRequest).then(res => {
            navigation.navigate('Address')
        }).catch(e => {
            console.log(e)
        })
    }

    if (latitude) {
        return (
            <Container>
                <HeaderTemplate secondaryHeader="gobackOnly" title="Cambia tu dirección" navigation={navigation} />
                <View style={{ flex: 1 }}>
                    <MapView
                        showsUserLocation
                        style={{ flex: 1 }}
                        initialRegion={{
                            latitude,
                            longitude,
                            latitudeDelta: 0.0022,
                            longitudeDelta: 0.0021
                        }}
                        onRegionChangeComplete={onRegionChange.bind(this)}
                    >
                    </MapView>

                    <View style={styles.markerFixed}>
                        <Image style={styles.marker} source={marker} />
                    </View>



                </View>
                <View style={styles.footer}>
                    <View style={styles.inputText}>
                        <Input
                            placeholder='dirección seleccionada'
                            keyboardType="default"
                            value={inputText}
                            onChangeText={onChangeAddress.bind(this)}
                            onSubmitEditing={() => { }}
                        />
                    </View>
                    <Button
                        title="Agregar dirección"
                        onPress={addAddress.bind(this)}
                    />
                </View>
            </Container>

        );
    }
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Necesitamos tu permiso</Text>
        </View>
    );

}