import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  content: {
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  gps: {
    paddingTop: 30
  },
  listItem: {
  },
  markerFixed: {
    left: '50%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%'
  },
  marker: {
    height: 48,
    width: 48
  },
  footer: {
    bottom: 0,
    position: 'absolute',
    width: '100%'
  },
  region: {
    position: 'absolute',
    lineHeight: 15,
    margin: 20
  },
  inputText: {
    backgroundColor: '#ffff',
    padding: 20
  },
  overlay: {
    height: '20%',
    borderRadius: 10
  },
  question:{
    paddingTop: 10,
    paddingLeft: 10,
    padding: '1%'
  },
  options: {
    paddingTop: 35,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center'
  },
  button: {
    paddingLeft: '10%',
    paddingRight: '10%'
  }
})