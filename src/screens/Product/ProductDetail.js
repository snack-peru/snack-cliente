
import React, { useState, useEffect } from 'react';
import { Text, View, Image, Button, Dimensions } from 'react-native';
import axios from '../../axios';
import styles from './ProductDetail.style';
import { Toast, Root } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage'
import HeaderTemplate from '../../navigations/Headers/Hamburger'
import { useFocusEffect } from '@react-navigation/native';

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

export default function ProductDetail({ route, navigation }) {

  /* 1. Get the param */
  const { productId } = route.params;
  const [quantity, setQuantity] = useState(1);

  const [product, setProduct] = useState({});
  const [aproxPrice, setAproxPrice] = useState([]);
  let productToPost = [{ productId: JSON.stringify(productId) }];
  

  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;
    setQuantity(1);
    showToast: false;
    axios.post("/api/customer/ListAproxPrice/", productToPost)
      .then(response => {
        setAproxPrice(response.data[0].aproxPrice)
      })
    axios.get('/api/customer/product/' + JSON.stringify(productId))
      .then(response => {
        if (response.data.image !== null) {
          let imageProductUrl = axios.defaults.baseURL + "/api/customer/product/image/" + response.data.image;
          response.data.image = imageProductUrl;
        } else {
          response.data.image = "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png";
        }
        setProduct(response.data);

      })
      .catch(error => {
        console.log(error);
      })
      return () => {
        isActive = false;
      };
    }, [route])
  );

  async function SendToBasket(tbQuantity, tbIdProduct, tbBrand, tbProductName, tbMeasurement) {
    let idCustomer = parseInt(await AsyncStorage.getItem('idCustomer'));
    var basket = {
      productId: tbIdProduct,
      name: tbProductName,
      brand: tbBrand,
      quantity: tbQuantity,
      measurement: tbMeasurement
    };
    try {
      await axios.post("/api/customer/basket/" + idCustomer, basket);
      console.log("Se mando a la cesta");
    }
    catch (error) {
      console.log("no se pudo mandar a la cesta");
    }
  }


  function increase(qty) {
    setQuantity(qty + 1);
  }

  function descrease(qty) {
    if (qty > 1) {
      setQuantity(qty - 1);
    }
  }



  function calculatePrice() {
    try {
      let total = aproxPrice * quantity;
      total = total.toFixed(2);
      return total;
    } catch (error) {
      console.log("error");
    }
  }

  let brandName;
  let unitOfMeasurement;

  if (JSON.stringify(product) === JSON.stringify({})) {
    brandName = <View style={styles.viewbrand}>
      <Text style={styles.textbrand}>Cargando marca...</Text>
    </View>
    unitOfMeasurement = <Text style={{ fontSize: 12 }}>Cargando unidad de medida...</Text>
  } else {
    brandName = <View style={styles.viewbrand}>
      <Text style={styles.textbrand}>{product.brand.name}</Text>
    </View>
    unitOfMeasurement = <Text style={{ fontSize: 23 }}>{product.unitOfMeasurement.name}</Text>
  }


  function AñadirACesta(quantityProduct) {

    SendToBasket(quantityProduct, product.idProduct, product.brand.name, product.name, product.unitOfMeasurement.name);

    if (quantityProduct == 1) {
      Toast.show({
        text: "Se añadio " + quantityProduct + " unidad a la cesta",
        duration: 3000,
        type: "success"
      })
    }
    else {
      if (quantityProduct > 1) {
        Toast.show({
          text: "Se añadieron " + quantityProduct + " unidades a la cesta",
          duration: 3000,
          type: "success"
        })
        setQuantity(1);
      }
      else {

      }
    }
  }

  return (
    <Root>
      <HeaderTemplate secondaryHeader="search" title="Detalle de producto" navigation={navigation} />
      <View style={styles.container}>
        <Image source={{ uri: product.image }}
          style={{ width: deviceWidth * 80 / 100, height: deviceHeight * 40 / 100, resizeMode: "contain" }} />

        <View style={styles.detail}>

          {brandName}

          <Text style={styles.textlabel}>{product.name}</Text>

          <View style={{ padding: 20, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
            <Text style={{ fontSize: 20, paddingRight: 10, }}>Cantidad:</Text>
            <Button style={styles.btncontrol} title="-" onPress={() => descrease(quantity)} />
            <Text style={styles.btncontrol}>{quantity} {unitOfMeasurement}</Text>
            <Button style={styles.btncontrol} title="+" onPress={() => increase(quantity)}></Button>
          </View>

          <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
            <Text style={{ fontSize: 20, paddingRight: 10, }}>Precio aprox:</Text>
            <Text style={{ fontSize: 25, paddingRight: 10, }}>S/. {calculatePrice()}</Text>
          </View>

          <View style={{ paddingTop: 25, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
            <View style={{ padding: 10 }}>
              <Button style={{ textAlign: 'center', fontSize: 35, }} title="Comprar ahora" onPress={() => alert('Se compró ahora')}></Button>
            </View>

            <View style={{ padding: 10 }}>
              {/*<Button style={{ textAlign: 'center', fontSize: 35, }} title="Añadir a cesta" onPress={() => alert('Se añadió a la cesta')}></Button>*/}
              <Button style={{ textAlign: 'center', fontSize: 35, }} title="Añadir a cesta" onPress={() => AñadirACesta(quantity)}></Button>
            </View>

          </View>
        </View>

      </View>
    </Root>
  );

}

