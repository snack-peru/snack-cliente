import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        flex: 1
      },
      detail: {
        paddingRight: 30,
        paddingLeft: 30,
        paddingTop: 15,
      },
      viewbrand: {
        paddingRight: 25,
        paddingLeft: 25,
        paddingTop: 2,
      },
      textbrand: {
        textTransform: 'uppercase',
        textAlign: 'center',
        fontSize: 20,
      },
      textlabel: {
        textAlign: 'center',
        fontSize: 23,
        fontWeight: 'bold'
      },
      viewdescription: {
        paddingLeft: 20,
        paddingTop: 20,
      }, 
      textdescription: {
        fontSize: 17,
      },
      btncontrol: {
        textAlign: 'center',
        width: 120,
        fontSize: 30,
      }
});