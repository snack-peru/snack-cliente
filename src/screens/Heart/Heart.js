import React from 'react';
import { Text, View } from 'react-native';
import { Container } from 'native-base';
import styles from './Heart.styles';

import HeaderTemplate from '../../navigations/Headers/Hamburger';

export default function Heart({ navigation }) {
  return (
    <Container>
      <HeaderTemplate title='Snack' subtitle='' navigation={navigation} />

      <View style={styles.container}>
          <Text>Tiendas favoritas!</Text>
        </View>
    </Container>
  );
}
