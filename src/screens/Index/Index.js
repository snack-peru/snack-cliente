import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, TouchableHighlight } from 'react-native';

const { width, height } = Dimensions.get('window');

export default function Index({ navigation: { navigate } }) {
  const [isLoggedIn, setIsLoggedIn] = useState(0);

  const changeIsLoggedIn = type => {
    if (type === 'google') {
      setIsLoggedIn(1);
    } else if (type === 'fb') {
      setIsLoggedIn(2);
    } else if (type === 'correo') {
      setIsLoggedIn(3);
    } else if (type === 'other') {
      setIsLoggedIn(4);
    }
  }

  if (!isLoggedIn) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          justifyContent: 'flex-end'
        }}
      >
        <View style={{ ...StyleSheet.absoluteFill }}>
          <Image
            source={require('../../../assets/login-bg.jpg')}
            style={{ flex: 1, height: null, width: null }}
          />
        </View>

        <View style={{ height: height / 1.8, justifyContent: 'center' }}>
          <View style={{ ...styles.button, backgroundColor: 'transparent' }}>
            <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#EFE8E7' }}>
              Ingresa a Snack!
              </Text>
          </View>

          <TouchableHighlight
            style={{ ...styles.button, backgroundColor: '#db4a39' }}
            activeOpacity={1}
            underlayColor='#ff0000'
            onPress={() => { }}
          >
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
              Google
            </Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={{ ...styles.button, backgroundColor: '#2E71DC' }}
            activeOpacity={1}
            underlayColor='#00008b'
            onPress={() => { }}
          >
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
              Facebook
            </Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.button}
            activeOpacity={1}
            underlayColor='#f0f8ff'
            onPress={() => navigate('EmailLogin')}
          >
            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Correo electrónico</Text>
          </TouchableHighlight>

          <View
            style={{
              paddingTop: 10,
              marginLeft: 10,
              marginRight: 10,
              borderBottomColor: 'white',
              borderBottomWidth: StyleSheet.hairlineWidth,
              fontWeight: 'bold'
            }}
          />

          <TouchableOpacity
            style={{ ...styles.button, backgroundColor: 'transparent' }}
            onPress={() => navigate('Home')}
          >
            <Text style={{ fontSize: 20, color: '#EFE8E7' }}>
              Crear más tarde
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: 'white',
    height: 55,
    marginHorizontal: 20,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5
  }
});
