import React, { useState, useEffect, Text } from 'react';
import { View } from 'react-native';
import { Body, Left, Button, Icon, Title, Container, Header, Tab, Tabs, ScrollableTab, Right } from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { LoadingIndicator } from '../../components/Shared/LoadingIndicator';
import HeaderTemplate from '../../navigations/Headers/Hamburger'

import axios from '../../axios';
import ProductList from '../../components/ProductList/ProductList';

export default function Subcategory({ route, navigation }) {

	/* 1. Get the param */
	const { categoryId } = route.params;
	const { categoryName } = route.params;

	const [isLoading, setIsLoading] = useState(true);
	const [subcategories, setSubcategories] = useState([]);
	const [mapPrices, setMapPrices] = useState(new Map());
	let mapTmp = new Map();
	let lsProductsId = [];

	useEffect(() => {
		axios.get("/api/customer/category/" + categoryId + "/subcategories").then(res => {
			setIsLoading(true);
			res.data.map((item1) => {
				item1.products.map((item2) => {
					if (item2.image !== null) {
						let imageProductUrl = axios.defaults.baseURL + "/api/customer/product/image/" + item2.image;
						item2.image = imageProductUrl;
					} else {
						item2.image = "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png";
					}
					lsProductsId.push({ productId: item2.idProduct });
					return item2;
				})

				axios.post("/api/customer/ListAproxPrice/", lsProductsId).then(res => {
					res.data.map((item3) => {
						mapTmp.set(item3.productId, item3.aproxPrice);
					})
					setMapPrices(mapTmp);
				})

				return item1;
			})

			setSubcategories(res.data);
			setIsLoading(false);
		});

	}, []);

	if (isLoading) {
		return (
			<Container>
				<LoadingIndicator />
			</Container>
		);

	} else {
		return (
			<Container>
      			<HeaderTemplate secondaryHeader="search" title={categoryName} navigation={navigation} />
				<Tabs renderTabBar={() => <ScrollableTab />}>
					{
						subcategories.map((sb, index) => {
							return (
								<Tab key={index} heading={sb.subcategory.name}>
									<View style={{ padding: 10, }}>
										{sb.products.map((product, index) => {
											let pricetmp;
											if (mapPrices.get(product.idProduct)) {
												pricetmp = mapPrices.get(product.idProduct);
											} else {
												pricetmp = undefined;
											}
											return (
												<TouchableNativeFeedback key={index} onPress={() => navigation.navigate('ProductDetail', { productId: product.idProduct })}>
													<ProductList product={product} price={pricetmp} />
												</TouchableNativeFeedback>
											);
										})}
									</View>
								</Tab>
							);
						})
					}
				</Tabs>
			</Container>
		);
	}
}
