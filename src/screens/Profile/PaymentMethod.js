import React from 'react';
import { Text, View } from 'react-native';
import { Container } from 'native-base';
import styles from './Profile.styles';

import HeaderTemplate from '../../navigations/Headers/Hamburger';

export default function PaymentMethod({ navigation }) {
  return (
    <Container>
      <HeaderTemplate title='Snack' subtitle='' navigation={navigation} />

      <View style={styles.container}>
          <Text>Metodos de pago del usuario!</Text>
        </View>
    </Container>
  );
}