import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    formContainer: {
        paddingTop: 60,
        paddingBottom: 40,
        paddingLeft: 15,
        paddingRight: 30
    },
    buttonContainer: {
        paddingLeft: "5%",
        padding: "5%",
        alignItems: 'center'
    }
});