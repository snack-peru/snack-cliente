import React, { useState } from 'react';
import { View, ScrollView, TouchableOpacity, Keyboard, TouchableWithoutFeedback } from 'react-native';
import { Container, Text, Icon } from 'native-base';
import { isEmptyString, isInvalidPhone, isInvalidEmail } from "../../utils/functions";
import styles from './EditProfile.styles';
import axios from '../../axios';
import AsyncStorage from '@react-native-community/async-storage';
import { SubmitButton } from '../../components/Shared/SubmitButton';
import { LoadingIndicator } from '../../components/Shared/LoadingIndicator';
import { InputWithValidation } from '../../components/Shared/InputWithValidation';
import { InputWithTimepicker } from '../../components/Shared/InputWithTimepicker';
import { useFocusEffect } from '@react-navigation/native';
import moment from 'moment';
import HeaderTemplate from '../../navigations/Headers/Hamburger'

import { logger } from 'react-native-logs';
var log = logger.createLogger();

export default function EditProfile({ navigation }) {
  const [isLoading, setIsLoading] = useState(true);

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [userName, setUserName] = useState('');
  const [userPhone, setUserPhone] = useState('');
  const [userEmail, setUserEmail] = useState('');
  const [userBirth, setUserBirth] = useState('');

  const [currentData, setCurrentData] = useState('');

  const setUserInfo = (data) => {
    setUserName(data.name);
    setUserPhone(data.phone);
    setUserEmail(data.email);
    setUserBirth(data.birthDate);

    setCurrentData(data);
  }

  const fetchData = async () => {
    const idCustomer = await AsyncStorage.getItem('idCustomer');
    if (idCustomer) {
      try {
        setIsLoading(true);
        log.debug("FETCH DATA ON: /api/customer/" + idCustomer);
        const response = await axios.get("/api/customer/" + idCustomer);
        setUserInfo(response.data.user);
        setIsLoading(false);
        log.debug("RESPONSE: ", response.data);
      } catch (e) {
        log.error("ERROR ON FETCH DATA", e);
      }
    }
  }

  const HasChanged = (data, field) => {
    if (currentData[field] === data) return false;
    return true;
  }

  const updateData = async () => {
    try {
      setIsLoading(true);
      const idCustomer = await AsyncStorage.getItem('idCustomer');
      const body = {};
      body.idUser = idCustomer ? idCustomer : null;
      if (HasChanged(userName, "name")) body.name = userName;
      if (HasChanged(userEmail, "email")) body.email = userEmail;
      if (HasChanged(userPhone, "phone")) body.phone = userPhone;
      if (HasChanged(userBirth, "birthDate")) body.birthDate = userBirth;

      log.debug("UPDATE DATA ON: /api/customer/update");
      log.debug("BODY:", body);
      const response = await axios.post("/api/customer/update", body);
      setUserInfo(response.data);
      log.debug("RESPONSE: ", response.data);
      setIsLoading(false);
    } catch (e) {
      log.error("ERROR ON UPDATE DATA", e);
    }
  }

  const enableButton = () => {
    if ((HasChanged(userName, "name")
      || HasChanged(userEmail, "email")
      || HasChanged(userPhone, "phone")
      || dateHasChanged())
      && !isEmptyString(userName) && !isEmptyString(userPhone)
      && !isEmptyString(userEmail) && !isInvalidEmail(userEmail)
      && !isInvalidPhone(userPhone)) {
      return true;
    }
    return false;
  }

  useFocusEffect(
    React.useCallback(() => {
      fetchData();
    }, [])
  );

  const showDatePicker = () => {
    setDatePickerVisibility(true);
    Keyboard.dismiss();
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    hideDatePicker();
    setUserBirth(date);
  };

  const validName = () => {
    return !isEmptyString(userName);
  }

  const validEmail = () => {
    return !(isEmptyString(userEmail) || isInvalidEmail(userEmail));
  }

  const validPhone = () => {
    return !(isEmptyString(userPhone) || isInvalidPhone(userPhone));
  }

  const dateHasChanged = () => {
    return (currentData.birthDate ? moment(currentData.birthDate).format('DD/MM/YYYY') : '') !== (userBirth ? moment(userBirth).format('DD/MM/YYYY') : '')
  }

  if (isLoading) {
    return (
      <Container>
        <LoadingIndicator />
      </Container>
    );

  } else {
    return (
      <Container>
        <ScrollView>
        <HeaderTemplate secondaryHeader="gobackOnly" title="Editar perfil" navigation={navigation} />

          <View style={styles.formContainer}>
            <InputWithValidation value={userName} label={"Nombre"}
              icon="person"
              isInitialValue={currentData.name === userName}
              handleOnChange={setUserName}
              isValid={validName()}
              validationMessage={"Campo no puede estar vacio"} />

            <InputWithValidation value={userPhone} label={"Teléfono celular"}
              icon="ios-phone-portrait"
              isInitialValue={currentData.phone === userPhone}
              handleOnChange={setUserPhone}
              isValid={validPhone()}
              validationMessage={"Campo invalido"}
              keyboardType={"phone-pad"} />

            <InputWithValidation value={userEmail} label={"Correo electrónico"}
              icon="mail"
              isInitialValue={currentData.email === userEmail}
              handleOnChange={setUserEmail}
              isValid={validEmail()}
              validationMessage={"Campo invalido"} />

            <InputWithTimepicker
              label="Fecha de nacimiento"
              isInitialValue={!dateHasChanged()}
              showDatePicker={showDatePicker}
              value={userBirth}
              isVisible={isDatePickerVisible}
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}>
            </InputWithTimepicker>

          </View>
        </ScrollView>
        {enableButton() &&
          <View style={styles.buttonContainer}>
            <SubmitButton name={'Actualizar'}
              handleButton={() => { updateData() }}
              buttonSize={'100%'}
              textSize={16}
              disabled={!enableButton()} />

          </View>
        }
      </Container>
    );
  }


}