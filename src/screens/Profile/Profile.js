import React from 'react';
import { Text, View } from 'react-native';
import { Container } from 'native-base';
import styles from './Profile.styles';

import HeaderTemplate from '../../navigations/Headers/Hamburger';

export default function Profile({ navigation }) {
  return (
    <Container>
      <HeaderTemplate title='Snack' subtitle='' navigation={navigation} />

      <View style={styles.container}>
          <Text>Perfil de usuario!</Text>
        </View>
    </Container>
  );
}