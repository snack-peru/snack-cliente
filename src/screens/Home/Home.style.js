import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    location: {
        fontSize: 16
    },
    container: {
        paddingTop: 5,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    storiesContainer: {
        paddingEnd: 10,
        paddingStart: 10,
        alignItems: 'center',
    },
    scrollContent: {
        justifyContent: 'space-evenly',
        width: 150,
        alignItems: 'center',
        alignSelf: 'center'
    },
    scrollImageView: {
        marginHorizontal: 5,
        borderColor: '#1A5276',
        borderWidth: 2,
        marginTop: 7,
        width: 145,
        height: 150,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    overlay: {
        height: 'auto',
        top: '30%',
        backgroundColor: 'rgba(211, 211, 211,  .7)',
        width: '80%',
        borderRadius: 5
    },
    scrollTextView: {
        fontSize: 17,
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center',
        height: 'auto'
    },
    imageContainer: {
        width: 120,
        height: 140,
        borderRadius: 10,
        position: 'absolute'
    },
    greetingView: {
        paddingTop: 10,
        paddingLeft: 30,
    },
    greeting: {
        fontSize: 30,
        fontWeight: 'bold'
    },
    address: {
        fontSize: 15,
        flexDirection: 'row',
        paddingTop: 2
    },
    categoriesTitleView: {
        paddingTop: 25,
        paddingLeft: 30
    },
    categoriesTitle: {
        fontSize: 25
    },
    verMasOption: {
        textAlign: 'right',
        paddingRight: 15
    }
});