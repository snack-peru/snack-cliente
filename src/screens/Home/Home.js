import React, { useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { Text, View, ScrollView } from 'react-native';
import { Container, Thumbnail, Icon } from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import styles from './Home.style';

import HeaderTemplate from '../../navigations/Headers/Hamburger';

import AsyncStorage from '@react-native-community/async-storage';
import axios from '../../axios';

export default function Home({ navigation }) {

  const [customerName, setCustomerName] = useState('');
  const [categories, setCategories] = useState([]);
  const [customerLocation, setCustomerLocation] = useState('Agregar dirección')

  useFocusEffect(
    React.useCallback(() => {
      fetchData();
    }, [])
  )

  const fetchData = async () => {
    axios.get("/api/customer/categoriesByRanking").then(res => {
      res.data.map((item, key) => {

        if (item.image !== null) {
          let imageCategoryUrl = axios.defaults.baseURL + "/api/customer/category/image/" + item.image;
          item.image = imageCategoryUrl;
          return item;
        } else {
          item.image = "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png";
          return item;
        }
      });

      //assign to categories
      setCategories(res.data);
    });

    getCustomerInfo();
    getLocation();

  };

  async function getCustomerInfo() {
    let name = await AsyncStorage.getItem('customerName');

    if (name !== null) {
      let indexSpace = name.indexOf(" ");
      if (indexSpace === -1) indexSpace = name.length;
      setCustomerName(name.substring(0, indexSpace))
      console.log(parseInt(await AsyncStorage.getItem('idUser')))
      console.log(parseInt(await AsyncStorage.getItem('idCustomer')))
      console.log(await AsyncStorage.getItem('customerEmail'))
      console.log(await AsyncStorage.getItem('customerPhone'))
    } else {
      setCustomerName("caser@")
    }

  }

  async function getLocation() {
    let idCustomer = await AsyncStorage.getItem('idCustomer')

    if (idCustomer !== null && idCustomer !== '' && idCustomer.length > 0) {
      axios.get("/api/customer/" + idCustomer + "/address/default").then(res => {
        // Se obtiene necesariamente una dirección
        setCustomerLocation(res.data.description)
        setLocationCache(res.data.description, res.data.latitude, res.data.longitude)
      })
    }
  }

  async function setLocationCache(description, latitude, longitude) {
    await AsyncStorage.setItem('customerLocationName', description)
    await AsyncStorage.setItem('customerLatitud', String(latitude))
    await AsyncStorage.setItem('customerLongitude', String(longitude))
  }

  return (
    <Container>
      <HeaderTemplate title='Snack' subtitle='' navigation={navigation} />

      <View style={styles.greetingView}>
        <Text style={styles.greeting}>¡Hola, {customerName}!</Text>
        <TouchableNativeFeedback style={styles.address} onPress={() => navigation.navigate('Address')}>
          <Icon active name='pin' />
          <Text style={styles.location}>  {customerLocation}</Text>
        </TouchableNativeFeedback>
      </View>

      <View style={styles.categoriesTitleView}>
        <Text style={styles.categoriesTitle}>Categorías</Text>

        <TouchableNativeFeedback onPress={() => navigation.navigate('Categories')}>
          <Text style={styles.verMasOption}>Ver todas</Text>
        </TouchableNativeFeedback>

      </View>

      <View style={styles.container}>

        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.storiesContainer}
        >
          {categories.map((category, index) => {
            return (
              <TouchableNativeFeedback key={index} onPress={() => navigation.navigate('Subcategory', { categoryId: category.idProductCategory, categoryName: category.name })}>
                <View style={styles.scrollContent} >
                  <View style={styles.scrollImageView}>
                    <Thumbnail
                      source={{ uri: category.image }}
                      style={styles.imageContainer}
                    />
                    <View style={styles.overlay}>
                      <Text style={styles.scrollTextView} numberOfLines={2}>{category.name}</Text>
                    </View>

                  </View>

                </View>
              </TouchableNativeFeedback>

            );
          })
          }
        </ScrollView>

      </View>

    </Container>
  );
}