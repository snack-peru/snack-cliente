import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },

    list: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        alignContent: "center",
        justifyContent: "space-around",
        padding: 10,
        paddingBottom: 40,
        paddingVertical: 30,
        paddingTop: 10,
        alignContent: "space-around",
        flexWrap: "wrap"
    },

    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },

      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      }
});