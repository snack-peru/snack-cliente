import React, { useState, useEffect } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { Container, Header, Left, Button, Icon, Body, Title, Right } from 'native-base';
import styles from './Categories.styles';
import { TouchableNativeFeedback } from 'react-native-gesture-handler'

import CardView from '../../components/Card/Card'
import ModalSearch from '../../components/Modals/Search'
import HeaderTemplate from '../../navigations/Headers/Hamburger'

import axios from '../../axios';

export default function Categories({ navigation }) {

  const [categories, setCategories] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {

    axios.get("/api/customer/categories").then(res => {
      res.data.map((item, key) => {

        if (item.image !== null) {
          let imageCategoryUrl = axios.defaults.baseURL + "/api/customer/category/image/" + item.image;
          item.image = imageCategoryUrl;
          return item;
        } else {
          item.image = "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png";
          return item;
        }

      });

      //assign to categories
      setCategories(res.data);
    });

  }, []);


  return (
    <Container>
      <HeaderTemplate secondaryHeader="search" title="Categorías" navigation={navigation} />

      <View style={styles.list}>
        {categories.map((category, index) => {

          return (
            <TouchableNativeFeedback key={index} onPress={() => navigation.navigate('Subcategory', { categoryId: category.idProductCategory, categoryName: category.name })}>
              <CardView categories={category} />
            </TouchableNativeFeedback>

          );
        })}


      </View>

      <ModalSearch modalVisible={modalVisible} setModalVisible={setModalVisible} />
    </Container>
  );
}
