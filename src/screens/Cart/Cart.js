import React, { useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { Container , Root, Toast, Content} from 'native-base';
import styles from './Cart.style';

import HeaderTemplate from '../../navigations/Headers/HamburgerCart';
import axios from '../../axios';
import ProductList from '../../components/Cart/ProductList';
import AsyncStorage from '@react-native-community/async-storage';
import BlockCartTop from '../../components/Cart/BlockCartTop';
import BlockCartBottom from '../../components/Cart/BlockCartBottom';
import { LoadingIndicator } from '../../components/Shared/LoadingIndicator';


export default function Cart({ navigation }) {

  const [total, setTotal] = useState(0.0);
  const [basketFinal, setBasketFinal] = useState([]);
  const [numcesta, setNumcesta] = useState(0);
  const [editBasket,setEditBasket] = useState('Editar')
  const [productsToEliminate,setProductsToEliminate] = useState([]);
  const [numSelectToEdit,setnumSelectToEdit]=useState(0);
  const [isLoading, setIsLoading] = useState(true);

  const setStateB = (stateB) =>{ 
    console.log("PasoSetState");
    if(stateB === 'Canasta final'){
      console.log("Editando");
      setEditBasket('Cancelar');
    }
    else{
      console.log("Final");
      setEditBasket('Editar');
      setnumSelectToEdit(0);
      basketFinal.forEach(element => {
        element.selected=false;
      })
      productsToEliminate.forEach(element => {
        element.selected=false;
      })
    }
  }

  const setSelectedProducts=(index,item,status) =>{
    console.log("Llego a carrito")
    console.log(index)
    console.log(item)
    console.log(status)
    basketFinal[index].selected=status;

    productsToEliminate[index].selected=status;
    if(status==true){
      setnumSelectToEdit(numSelectToEdit+1);
    }
    else{
      setnumSelectToEdit(numSelectToEdit-1);
    }
    console.log(productsToEliminate);
    //cnsole.log(numSelectToEdit);

    
  } 

  const selectAllProducts = () => {
    let i=0;
    console.log("Entro a selectAllProducts");
    console.log(basketFinal);
    basketFinal.forEach(element => {
      if(element.selected==false){
        element.selected=true;
      }
    })
    productsToEliminate.forEach(element =>{
      if(element.selected==false){
        element.selected=true;
        i=i+1;
      }
    })

    setnumSelectToEdit(numSelectToEdit+i);
    console.log(numSelectToEdit);
    console.log(productsToEliminate);
  }

  async function deleteProducts(productsToDelete) {
    let i=numSelectToEdit;
    let idCustomer = parseInt(await AsyncStorage.getItem('idCustomer'));
    console.log(parseInt(await AsyncStorage.getItem('idCustomer')));
    try{
      await axios.put("/api/customer/basket/"+ idCustomer,productsToDelete)
      console.log("Exito al eliminar productos");
    }
    catch (error){
      console.log("Error al eliminar productos")
      console.log(error);
    }
    try{
      setEditBasket('Editar');
      setnumSelectToEdit(0);
      setBasketFinal([]);
      setProductsToEliminate([]);
      setNumcesta(0);
      setTotal(0.0);
      await fetchProducts();
      await fetchPrice();
    }
    catch(error){
      console.log("Error al intentar refrescar la cesta");
      console.log(error)
    }
    try{
      console.log(i)
      if(i == 1){
        Toast.show({
          text: "Se elimino " +numSelectToEdit+ " producto de la cesta" ,
          duration: 3000,
          type: "success"
        })
      }
      else{
        if (i >1){
          Toast.show({
            text: "Se eliminaron " +numSelectToEdit+ " productos de la cesta" ,
            duration: 3000,
            type: "success"
          })
        }
        else{
  
        }
      }
    }
    catch(error){
      console.log("Error al sacar el toast");
      console.log(error)
    }

  }

  const deleteProductsSelected = () => {
    console.log("Entro a deleteProductsSelected");
    let arrayOfIdProducts=[];
    basketFinal.forEach(element => {
      if(element.selected==true){
      arrayOfIdProducts.push(element.productId);
      }
    })
    console.log(arrayOfIdProducts)
    let productsToDelete = ({listProducts : arrayOfIdProducts});
    console.log (productsToDelete);
    if(arrayOfIdProducts.length>0){
      console.log("Hay productos para eliminar")
      deleteProducts(productsToDelete);
    }
    else{
      console.log("No hay productos que eliminar")
    }
    //const response = await axios.post("/api/customer/ListAproxPrice/", productsAloneOfBasket);
    
  }

  //Esta funcion añadira al arreglo basketFinal varios objetos que tendran los campos id,productId,brand,etc..
  const AddingToBasketFinal = (basket, aproxPriceB, totalPriceProductB) => {
    setBasketFinal(currentBasket => [...currentBasket, { id: Math.random().toString(), productId: basket.productId, brand: basket.brand, measurement: basket.measurement, name: basket.name, quantity: basket.quantity, aproxPrice: aproxPriceB, totalPriceProduct: totalPriceProductB, selected:false }])
    setProductsToEliminate(currentSetProductsToEliminate => [...currentSetProductsToEliminate, {productId: basket.productId, selected:false}])
  }

  let productsAloneOfBasket;

  //funcion que llama y muestra la cesta
  async function fetchProducts() {
    setIsLoading(true);
    //se trae la cesta del usuario
    let idCustomer = parseInt(await AsyncStorage.getItem('idCustomer'));
    console.log(parseInt(await AsyncStorage.getItem('idCustomer')));
    try {
      const res = await axios.get("/api/customer/basket/" + idCustomer)
      //el resultado se setea en esta variable que sera usada luego por la cesta final y para calcular el precio total de cad aproducto
      productsAloneOfBasket = res.data.productsAlone
    }
    catch (error) {
      console.log("No se encontro canasta de ese usuario")
    }
    setIsLoading(false);
  }



  //funcion que llama y muestra los precios aproximados de cada producto de la cesta
  async function fetchPrice() {
    setIsLoading(true);
    console.log(productsAloneOfBasket);
    let i = 0;
    let basketTotalPrice = 0.0
    try {
      // se trae la lista de los precios aproximados con sus productId
      const response = await axios.post("/api/customer/ListAproxPrice/", productsAloneOfBasket);

      //const productResponse =  await fetchProduct(element.productId);
      //console.log(productResponse.data);


      //Hara lo siguiente por cada producto que tiene su precio aproximado
      response.data.forEach(element => {

        //Si el id del producto de este objeto que contiene el precio aproximado es igual al id del producto de la cesta ,indeice i, entonces ejecutamos lo siguiente
        if (element.productId == productsAloneOfBasket[i].productId) {

          //Suma los precios de cada producto hacia la variable del precio final de la cesta
          basketTotalPrice = basketTotalPrice + (element.aproxPrice * productsAloneOfBasket[i].quantity);


          //Halla el precio total aproximado de ese producto
          let totalPriceProduct = (element.aproxPrice * productsAloneOfBasket[i].quantity).toFixed(2);

          //Halla el precio aproximado por unidad de ese producto
          let aproxPriceProduct = element.aproxPrice;

          //Agrega a la cesta final que sera luego usada en los components
          AddingToBasketFinal(productsAloneOfBasket[i], aproxPriceProduct, totalPriceProduct);
        }
        else{
          console.log("Id del producto no coincide con el producto de la cesta");
        }

        i = i + 1;


      })
      //luego de hallar el precio total de la cesta , guardamos el numero total de lineas de producto que existen para luego imprimirlo en el header
      setNumcesta(i);
      //Ademas redondeamos a 2 decimales el precio final
      setTotal(basketTotalPrice.toFixed(2));


    }
    catch (error) {
      console.log("No se pudo encontrar precios aproximados")
    }

    setIsLoading(false);
    
  }

  //useFocusEffect se usa para que se ejecute lo que esta dentro cada vez que la cesta se recargue
  useFocusEffect(

    React.useCallback(() => {
      let isActive = true;

      async function fillBasket() {
        setEditBasket('Editar');
        setnumSelectToEdit(0);
        setBasketFinal([]);
        setProductsToEliminate([]);
        setNumcesta(0);
        setTotal(0.0);
        await fetchProducts();
        await fetchPrice();

      }
      fillBasket();
      console.log(navigation);
      console.log("rendering Cart");

      //se ejecutara 1 sola vez cada vez que se recargue la cesta
      return () => {
        isActive = false;
      };
    }, [navigation])
  );
  if (isLoading) {
    return (
      <Container>
        <LoadingIndicator/>
      </Container>
    );

  } else {
  return (
    <Root>
    <Container>
      <HeaderTemplate title='Cesta' subtitle='' numcesta={numcesta} navigation={navigation} onEditBasket={setStateB} editar={editBasket} numselect={numSelectToEdit}  />
      <BlockCartTop title={total} edit={editBasket} navigation={navigation} products={basketFinal}/>
      <ProductList title={basketFinal} edit={editBasket} selectProductFunction={setSelectedProducts}/>
      <BlockCartBottom title={total} edit={editBasket} selectAllFunction={selectAllProducts} deleteProducts={deleteProductsSelected}/>
    </Container>
    </Root>

  )
  }

}
