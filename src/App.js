import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderBackButton } from '@react-navigation/stack';

import Index from './screens/Index/Index';
import EmailLogin from './screens/EmailLogin/EmailLogin';
import Drawer from './navigations/DrawerStack/DrawerStack';
import CategoriesScreen from './screens/Categories/Categories';
import AddressScreen from './screens/Address/Address';
import NewAddressScreen from './screens/Address/NewAddress';
import SelectStoreScreen from './screens/Store/SelectStore';
import ProductDetailScreen from './screens/Product/ProductDetail';
import SubcategoryScreen from './screens/Subcategory/Subcategory';
import SearchScreen from './screens/Search/Search';
import AvailableProductStoreScreen from './screens/Store/AvailableProductStore';
import SearchStoreScreen from './screens/Search/SearchStore'
import StoreProfileScreen from './screens/Store/StoreProfile'

const Stack = createStackNavigator();

export default function App() {

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Index"
      screenOptions={{
        headerShown: false
      }}>
        <Stack.Screen options={{headerShown: false}} name="Index" component={Index} />
        <Stack.Screen name="EmailLogin" component={EmailLogin} />
        <Stack.Screen name="Home" component={Drawer} />
        <Stack.Screen name="Categories" component={CategoriesScreen} />
        <Stack.Screen name="Address" component={AddressScreen} />
        <Stack.Screen name="NewAddress" component={NewAddressScreen} />
        <Stack.Screen name="SelectStore" component={SelectStoreScreen} />
        <Stack.Screen name="Subcategory" component={SubcategoryScreen} />
        <Stack.Screen name="ProductDetail" component={ProductDetailScreen} />
        <Stack.Screen name="Search" component={SearchScreen} />
        <Stack.Screen name="AvailableProductStore" component={AvailableProductStoreScreen}/>
        <Stack.Screen name="SearchStore" component={SearchStoreScreen} />
        <Stack.Screen name="StoreProfile" component={StoreProfileScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
