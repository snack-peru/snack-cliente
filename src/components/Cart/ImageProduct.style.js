import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    thumbnailImageProduct: {
        width: 100, 
        height: 90,
        resizeMode: "contain"
    }
});