import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    viewProductList: {
        flexDirection: 'row', 
        padding: 20
    },
    viewQuantityAndMeasurement: {
        width: '8%', 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    viewCheckBoxes: {
        flexDirection: 'row', 
        width: '8%',
        height:'100%',
        justifyContent: 'flex-start', 
        alignItems: 'center' 
    },
    viewImageProduct:{
        width: '28%', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    viewProductInformation:{
        width: '48%', 
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    textProductName:{
        fontWeight: 'bold'
        
    },
    viewTotalPriceProduct:{
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    textTotalPriceProduct:{
        paddingLeft: 3, 
        paddingRight: 3, 
        paddingTop: 3, 
        paddingBottom: 3, 
        backgroundColor:'#68a0cf', 
        borderRadius: 10, 
        backgroundColor: '#dbdbdb'
    },
    textQuantityProduct:{
        fontWeight: 'bold',
        fontSize: 15
    }


});