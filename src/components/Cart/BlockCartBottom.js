import React,{Fragment} from 'react';
import {View,Text,Button} from 'react-native';
import styles from './BlockCartBottom.style';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';



const BlockCartBottom = props => {

    const selectAll = () => {
        console.log("Seleccionar Todo");
        props.selectAllFunction();
    }

    const deleteProducts = () => {
        console.log("Eliminar Productos");
        props.deleteProducts();
    }

    return (
        <View>
            {props.edit==='Editar' &&
            <Fragment>
            <View style={styles.viewBottomBlock}>
            <View style={styles.viewTotalPriceCart}>
                <Text>Total aprox.</Text>
                <Text style={styles.textTotalPriceCart}>S/. {props.title}</Text>
            </View>
            <View style={styles.viewGoToPay}>
                <Button title="Ir a pagos" color ="gray"  onPress={() => console.log("Ir a pagos")} />
            </View>
            </View>
            </Fragment>
            }
            {props.edit==='Cancelar' && 
                <Fragment>
                <View style ={styles.viewBottomBlockEdit}>
                <View style ={styles.viewSelectAll}>
                    <TouchableNativeFeedback onPress={()=> selectAll()}>
                        <MaterialCommunityIcons name="check-box-multiple-outline" size={26} style={styles.iconSelectAll} />
                        <Text style={styles.textSelectAll}>Seleccionar Todo</Text>
                    </TouchableNativeFeedback>
                </View>

                <View style ={styles.viewDeleteProducts}>
                    <TouchableNativeFeedback onPress={()=> deleteProducts()}>
                        
                        <Ionicons name="md-trash" size={26} style={styles.iconDeleteProducts} />
                        <Text style={styles.textDeleteProducts}>Eliminar</Text>
                    </TouchableNativeFeedback>
                </View>
                </View>
                </Fragment>
            }
        </View>
    )
};

export default BlockCartBottom;