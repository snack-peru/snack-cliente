import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    viewBlockTop: {
        flexDirection: 'row',
        padding: 10,
        borderColor: 'black',
        borderWidth: 1,
        justifyContent: 'space-between'
    },
    viewTotalPriceCartTop: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    textTotalPriceCartTop: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 3, 
        paddingBottom: 3, 
        backgroundColor: '#68a0cf', 
        borderRadius: 10, 
        backgroundColor: '#dbdbdb', 
        fontSize: 16
    },
    viewTopBlockEdit:{
        flexDirection: 'row',
        padding:10,
        borderColor: 'black',
        borderWidth: 1,
    },
    textTopBlockEdit:{
        fontSize:20
    }
});