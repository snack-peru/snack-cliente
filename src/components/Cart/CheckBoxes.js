import React , {useState,useEffect} from 'react';
import {Thumbnail} from 'native-base';
import axios from '../../axios';
import styles from './ImageProduct.style';
//import { CheckBox } from 'native-base';
import CheckBox from '@react-native-community/checkbox'


const CheckBoxes = props => {

    const [stateCheckBox, setStateCheckBox] = useState();
    
    const checkboxChecked = (index,item) => {
        console.log("Entro a checkboxChecked");
        console.log(props.index)
        console.log(item)
        if(props.title.selected==false){
            //setStateCheckBox(true);
            props.functionChecked(index,item,true)
        }
        else{
            //setStateCheckBox(false);
            props.functionChecked(index,item,false)
        }
    }
  
      useEffect(() => {
            //console.log("Rendering CheckBoxes");
            //console.log(props.title)
            setStateCheckBox(props.title.selected);
        }, []);
      return(
        <CheckBox value={props.title.selected} onValueChange={() => checkboxChecked(props.index,props.title)}/>
      )
  };
  
  export default CheckBoxes;