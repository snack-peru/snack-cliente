import React, { useEffect, Fragment,useState } from 'react';

import { Text, View, FlatList, Image} from 'react-native';
import ImageProduct from './ImageProduct';
import styles from './ProductList.style';
import CheckBoxes from './CheckBoxes';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
let check='false'

const ProductList = props => {



  const [checkboxes,setCheckboxes]=useState(props.title);
  const setProductChecked = (index,item,status) => {
    console.log("Entro a productList")
    console.log(index)
    console.log(item)
    console.log(status)
    props.selectProductFunction(index,item,status);
  }



  //Recibe la cesta mandada desde cart en props.title
  useEffect(() => {
    console.log("rendering Productlist");
    console.log(props.title);
    //setCheckboxes(props.title);
  }, []);
  return (
    <FlatList keyExtractor={(item, index) => item.id} data={props.title} renderItem={itemData => (
      <View style={styles.viewProductList}>
          {props.edit==='Editar' &&
            <View style={styles.viewQuantityAndMeasurement}>
            <Fragment>
            <Text style={styles.textQuantityProduct}>{itemData.item.quantity}</Text>
            <Text>{itemData.item.measurement}</Text>
            </Fragment>
            </View>
          }
          {props.edit==='Cancelar' &&
            <Fragment >
            <View  style={styles.viewCheckBoxes}>
            <CheckBoxes title={itemData.item} functionChecked={setProductChecked} index={itemData.index}/>
            </View>
            </Fragment>
          }
            
        <View style={styles.viewImageProduct}>
          <ImageProduct title={itemData.item.productId} />
        </View>
        <View style={styles.viewProductInformation}>
          <Text>{itemData.item.brand}</Text>
          <Text style ={styles.textProductName}>{itemData.item.name}</Text>
          <Text>S/.{itemData.item.aproxPrice.toFixed(2)} x {itemData.item.measurement}</Text>
        </View>
        <View style={styles.viewTotalPriceProduct}>
          <Text>P.aprox:</Text>
          <Text style={styles.textTotalPriceProduct}>S/.{itemData.item.totalPriceProduct}</Text>
        </View>
      </View>)}
    />
  )

};

export default ProductList;