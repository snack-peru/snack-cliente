import React,{Fragment} from 'react';
import {View,Text,Button} from 'react-native';
import styles from './BlockCartTop.style';
import Feather from 'react-native-vector-icons/Feather';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';


const BlockCartTop = (props) => {

    const onPress = () => {
        const products = props.products.map((p) => {
            console.log(p.productId)
            return {
                productId: p.productId,
                name: p.name,
                measurement: p.measurement,
                brand: p.brand,
                quantity: p.quantity
            } 
        })
        console.log("Products: ", props.products)
        props.navigation.navigate(
            "SelectStore", 
            { productList: products }
        )
    };

    return (
        <View>
            {props.edit==='Editar' &&
                <View style={styles.viewBlockTop}>
                <Fragment>
                <Button title="Elegir tienda" onPress={onPress} />
                <View style={styles.viewTotalPriceCartTop}>
                    <Text style={styles.textTotalPriceCartTop}>S/. {props.title}</Text>
                </View>
                </Fragment>
                </View>
            }
            {props.edit==='Cancelar' && 
                <Fragment>
                <TouchableNativeFeedback>
                <View style={styles.viewTopBlockEdit}>
                <View style={{width: '10%'}}>
                <Feather name="minus-circle" size={26} />
                </View>
                <View style={{width: '80%'}}>
                <Text style={styles.textTopBlockEdit}>Productos sin tienda</Text>
                </View>
                </View>
                </TouchableNativeFeedback>
                </Fragment>
                
            }
        </View>
    )
};

export default BlockCartTop;