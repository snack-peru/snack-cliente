import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    viewBottomBlock:{
        flexDirection: 'row',
        padding:10,
        borderColor: 'black',
        borderWidth: 1,
        justifyContent: 'space-between'
    },
    viewTotalPriceCart:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewGoToPay:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewSelectAll:{
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%'
    },
    viewDeleteProducts:{
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%'
    },
    textTotalPriceCart:{
        fontSize: 16, 
        fontWeight: 'bold'
    },
    viewBottomBlockEdit:{
        flexDirection: 'row',
        padding:10,
        borderColor: 'black',
        borderWidth: 1,
    },
    iconSelectAll:{
        width: '60%'
    },
    textSelectAll:{
        fontSize: 16
    },
    iconDeleteProducts:{
        alignSelf:'flex-end',
        width: '65%',
        //justifyContent: 'flex-end',
        //alignItems: 'flex-end',
    },
    textDeleteProducts:{
        fontSize: 16
        //justifyContent:'center',
        //alignItems: 'center'
    },
});