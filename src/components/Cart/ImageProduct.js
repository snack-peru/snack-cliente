import React , {useState,useEffect} from 'react';
import { View, Image } from 'react-native';
import {Thumbnail} from 'native-base';
import axios from '../../axios';
import styles from './ImageProduct.style';

const ImageProduct = props => {

  const [product, setProduct] = useState({});


    useEffect(() => {

      //Recibe el id del producto en props.title y obtiene todo el producto con el siguiente metodo API
      axios.get('/api/customer/product/'+props.title)
      .then(response => {
        //obtiene la imagen para luego imprimirla en el Thumbnail
        if (response.data.image !== null) {
          let imageProductUrl = axios.defaults.baseURL + "/api/customer/product/image/" + response.data.image;
          response.data.image = imageProductUrl;
        } else {
          response.data.image = "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png";
        }
        setProduct(response.data);
        
      })
      .catch(error => {
        console.log(error);
      })
        
          console.log("rendering ImageProduct");
    
      }, []);
      {/* <Thumbnail square large source={{ uri: product.image }} style={styles.thumbnailImageProduct} />  */}
      
    return(
      <Image style={{
        width: 70,
        height: 90,
        borderRadius: 10,
        resizeMode: "contain",
    }} source={{ uri: product.image}}></Image>
      
    )
};

export default ImageProduct;