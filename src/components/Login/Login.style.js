import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
    loginButton: {
        marginTop: 30,
        alignSelf: 'center'
    },
    loginText: {
        color: 'white',
        alignItems: 'center'
    },
    formConatiner: {
        paddingLeft: 15,
        paddingRight: 30
    },
    passwordForgottenText: {
        paddingTop: 20,
        paddingRight: 20,
        textAlign: 'right',
        color: 'purple'
    }
});