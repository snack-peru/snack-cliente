import React, { useState } from 'react';
import { Button, Text, Content, Form, Item, Label, Input, Toast, Root } from 'native-base';
import styles from './Login.style';
import * as Crypto from 'expo-crypto';

import AsyncStorage from '@react-native-community/async-storage';
import axios from '../../axios';

export default function Login({ navigation }) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    async function runCrypto() {
        const digest = await Crypto.digestStringAsync(
            Crypto.CryptoDigestAlgorithm.SHA256,
            password
        );
        return digest;
    }

    async function saveCustomerInfo(data) {
        await AsyncStorage.setItem('customerName', data.name)
        await AsyncStorage.setItem('idUser', String(data.idUser))
        await AsyncStorage.setItem('idCustomer', String(data.idCustomer))
        await AsyncStorage.setItem('customerEmail', data.email)
        await AsyncStorage.setItem('customerPhone', data.phone)
    }

    async function login() {
        //verificar que se cumplan las condiciones
        let emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if (email.length > 0 && password.length > 0) {
            if (emailRegex.test(email)) {
                // se verifica que el email tenga un formato válido

                let passwordEncrypted = await runCrypto();
                var user = {
                    email: email.toLowerCase(),
                    password: passwordEncrypted
                };
                
                axios.post("/api/customer/login", user).then(res => {
                    navigation.navigate("Home");
                    
                    // Se guardar en cache la información del usuario
                    saveCustomerInfo(res.data);

                }).catch(error => {
                    Toast.show({
                        text: error.response.data.message,
                        type: "danger",
                        duration: 10000
                    })
                })
            } else {
                Toast.show({
                    text: "El correo no tiene el formato correcto",
                    type: "danger",
                    duration: 10000
                })
            }
        } else {
            Toast.show({
                text: "Debe completar los campos de correo y contraseña",
                type: "danger",
                duration: 10000
            })
        }

    }

    return (
        <Root>
            <Content>
                <Form style={styles.formConatiner}>
                    <Item floatingLabel>
                        <Label>Correo</Label>
                        <Input onChangeText={(email) => { setEmail(email) }} keyboardType="email-address" />
                    </Item>
                    <Item floatingLabel>
                        <Label>Contraseña</Label>
                        <Input secureTextEntry={true} onChangeText={(password) => { setPassword(password) }} />
                    </Item>
                </Form>
                <Text style={styles.passwordForgottenText}>¿Olvidaste tu contraseña?</Text>
                <Button style={styles.loginButton} onPress={login.bind(this)} >
                    <Text style={styles.loginText}>Ingresar</Text>
                </Button>
            </Content>
        </Root>
    );
}