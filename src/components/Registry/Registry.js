import React, { useState } from 'react';
import { Button, Text, Content, Form, Item, Label, Input, Toast, Root } from 'native-base';
import styles from './Registry.style';
import * as Crypto from 'expo-crypto';

import axios from '../../axios';
import AsyncStorage from '@react-native-community/async-storage';

export default function Registry({ navigation }) {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordRepeated, setPasswordRepeated] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    async function runCrypto() {
        const digest = await Crypto.digestStringAsync(
            Crypto.CryptoDigestAlgorithm.SHA256,
            password
        );
        return digest;
    }

    async function saveCustomerInfo(data) {
        await AsyncStorage.setItem('customerName', data.name)
        await AsyncStorage.setItem('idUser', String(data.idUser))
        await AsyncStorage.setItem('idCustomer', String(data.idCustomer))
        await AsyncStorage.setItem('customerEmail', data.email)
        await AsyncStorage.setItem('customerPhone', data.phone)
    }

    async function registerUser() {
        //verificar que se cumplan las condiciones
        let emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if (name.length > 0 && email.length > 0 && password.length > 0 && passwordRepeated.length > 0 && phoneNumber.length > 0) {
            if (emailRegex.test(email)) {
                //se verifica que el email sea válido

                if (password.localeCompare(passwordRepeated) === 0) {
                    //se verifica que las contraseñas sean iguales
                    //se llama el método de registro del backend
                    let passwordEncrypted = await runCrypto();
                    var user = {
                        name: name,
                        phone: phoneNumber,
                        email: email,
                        password: passwordEncrypted
                    };
                    axios.post('/api/customer/register', user).then(res => {
                        navigation.navigate("Home");

                        // Se guardar en cache la información del usuario
                        saveCustomerInfo(res.data);

                    }).catch(error => {
                        Toast.show({
                            text: error.response.data.message,
                            type: "danger",
                            duration: 10000
                        })
                    })
                } else {
                    Toast.show({
                        text: "Las contraseñas no son iguales",
                        type: "danger",
                        duration: 10000
                    })
                }
            } else {
                Toast.show({
                    text: "Ingresar un correo válido",
                    type: "danger",
                    duration: 10000
                })
            }
        } else if (isNaN(phoneNumber)) {
            Toast.show({
                text: "El número de teléfono solo debe contener digitos",
                type: "danger",
                duration: 10000
            })
        } else {
            Toast.show({
                text: "Debe completar todos los campos",
                type: "danger",
                duration: 10000
            })
        }
    }

    return (
        <Root>
            <Content>
                <Form style={styles.formConatiner}>
                    <Item floatingLabel>
                        <Label>Nombre</Label>
                        <Input onChangeText={(name) => { setName(name) }} />
                    </Item>
                    <Item floatingLabel>
                        <Label>Correo</Label>
                        <Input onChangeText={(email) => { setEmail(email) }} keyboardType="email-address" />
                    </Item>
                    <Item floatingLabel>
                        <Label>Nueva Contraseña</Label>
                        <Input secureTextEntry={true} onChangeText={(password) => { setPassword(password) }} />
                    </Item>
                    <Item floatingLabel>
                        <Label>Repetir Contraseña</Label>
                        <Input secureTextEntry={true} onChangeText={(password) => { setPasswordRepeated(password) }} />
                    </Item>
                    <Item floatingLabel>
                        <Label>Número de telefono</Label>
                        <Input keyboardType="phone-pad" onChangeText={(phoneNumber) => { setPhoneNumber(phoneNumber) }} />
                    </Item>
                </Form>
                <Button style={styles.loginButton} onPress={registerUser.bind(this)} >
                    <Text style={styles.loginText}>Resgistrarse</Text>
                </Button>
            </Content>
        </Root>
    );
}