import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    overlay: {
        height: '20%',
        borderRadius: 10
    },
    question: {
        paddingTop: 10,
        paddingLeft: 10,
        padding: '1%'
    },
    options: {
        paddingTop: 35,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center'
    },
    button: {
        paddingLeft: '10%',
        paddingRight: '10%'
    }
})