import React, { useEffect, useState } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from "react-native";
import { SearchBar } from "react-native-elements";
import { TouchableNativeFeedback, ScrollView } from 'react-native-gesture-handler';

import styles from './Search.style';
import axios from '../../axios';
import ProductList from '../ProductList/ProductList';

const Search = (props) => {

  const [searchText, setSearchText] = useState('');
  const [products, setProducts] = useState([]);
	const [mapPrices, setMapPrices] = useState(new Map());
	let mapTmp = new Map();
	let lsProductsId = [];

  function searchProducts() {
        axios.get("/api/customer/product/search/" +  searchText).then(res => {
          res.data.map((item)=>{
            if (item.image !== null) {
              let imageProductUrl = axios.defaults.baseURL + "/api/customer/product/image/" + item.image;
              item.image = imageProductUrl;
            } else {
              item.image = "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png";
            }

            lsProductsId.push({ productId: item.idProduct });
            return item;
          });

          axios.post("/api/customer/ListAproxPrice/", lsProductsId).then(res => {
            res.data.map((item) => {
              mapTmp.set(item.productId, item.aproxPrice);
            })
            setMapPrices(mapTmp);
          });

          setProducts(res.data);
          
      }).catch(error =>{
        console.log(error);
      })
  }

  return (
    // <View style={styles.centeredView}>
    <View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={props.modalVisible}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            {/* <Text style={styles.modalText}>Busca productos!</Text> */}
            
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={() => {
                props.setModalVisible(!props.modalVisible);
              }}
            >
              <Text style={styles.textStyle}>Cerrar</Text>
            </TouchableHighlight>

              <SearchBar
                platform = "android"
                placeholder="Escribe Aqui..."
                onChangeText={setSearchText}
                value={searchText}
                />
            
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={searchProducts.bind(this)}
            >
              <Text style={styles.textStyle}>Buscar</Text>
            </TouchableHighlight>
            
            <ScrollView>
            <View>
                {products.map((product, index) => {
                  let pricetmp;
                  if (mapPrices.get(product.idProduct)) {
                    pricetmp = "S/." + mapPrices.get(product.idProduct);
                  } else {
                    pricetmp = "...";
                  }
                  return (
                    <TouchableNativeFeedback key={index} onPress={() => navigation.navigate('ProductDetail', { productId: product.idProduct })}>
                      <ProductList product={product} price={pricetmp}/>
                    </TouchableNativeFeedback>
                  );
                })}
            </View>

            </ScrollView>
            
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default Search;