import React, { useState, useEffect } from 'react';
import { Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import styles from './ConfirmationModal.style';
import { Overlay } from 'react-native-elements';

const ConfirmationModal = (props) => {

    const [isLoading, setIsLoading] = useState(false)

    function confirmButton() {
        setIsLoading(true)
        props.confirmAction()
        setIsLoading(false)
        props.toggleOverlay(props.value)
    }

    return (
        <Overlay
            isVisible={props.visible}
            onBackdropPress={props.toggleOverlay}
            overlayStyle={styles.overlay}
        >
            <View>
                <Text style={styles.question}>{props.question}</Text>
                <View style={styles.options}>
                    <View style={styles.button}>
                        <Button
                            title="Cancelar"
                            type="outline"
                            onPress={props.cancelAction.bind(this, null)}
                        />
                    </View>
                    <View style={styles.button}>
                        <Button
                            title="Confirmar"
                            loading={isLoading}
                            onPress={confirmButton}
                        />
                    </View>
                </View>
            </View>
        </Overlay>
    )
}

export default ConfirmationModal;