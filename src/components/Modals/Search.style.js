import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
export default StyleSheet.create({

    centeredView: {
        // flex: 1,
        // justifyContent: "center",
        // alignItems: "center",
        marginTop: 0
      },
      modalView: {
        // margin: 5,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        height: height,
        width: width,
        //alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },

      textProduct: {
        color: "black",
        fontWeight: "bold",
        textAlign: "center"
      },

      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }

});