import React from 'react';
import { StyleSheet, View} from 'react-native';
import { Item, Label, Input, Text, Icon } from 'native-base';

export const InputWithValidation = (props) => {
    return (
        <View style={styles.container}>
            <Item floatingLabel success={props.isValid && !props.isInitialValue} error={!props.isValid && !props.isInitialValue} >
                {props.icon? <Icon active name={props.icon} />: <></>}
                <Label style={props.isValid? styles.validText : styles.invalidText} > {props.label}</Label>
                <Input
                    style={props.isValid? styles.validText : styles.invalidText} 
                    keyboardType= {props.keyboardType}
                    value={props.value}
                    onChangeText={(newValue) => {                    
                        props.handleOnChange(newValue);
                    }} />
                { !props.isInitialValue ? <Icon name={!props.isValid? 'close-circle': 'checkmark-circle'} /> : <></>}                
            </Item>

            {!props.isValid && 
            <View style={styles.errorMessageContainer}>
                <Text style={styles.invalidText}>{props.validationMessage}</Text>
            </View>
            }        
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        paddingBottom: '15%'
    },
    invalidText: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#FF2D00'
    },
    validText: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#000000'
    },
    errorMessageContainer: {
        alignItems: 'flex-end'
    }
  });