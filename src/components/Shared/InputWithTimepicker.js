
import React from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, Keyboard} from 'react-native';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Item, Label, Input, Icon } from 'native-base';
import moment from 'moment';

export const InputWithTimepicker = (props) => {
    return (
        <View style={styles.container}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <Item floatingLabel success={!props.isInitialValue}>
                    <Icon active name={'calendar'} />
                    <Label>{props.label}</Label>     
                    <Input caretHidden 
                        value={props.value? moment(props.value).format('DD/MM/YYYY'): ''}
                        onFocus={props.showDatePicker}
                        />
                    { !props.isInitialValue ? <Icon name={'checkmark-circle'} /> : <></>}
                </Item>
            </TouchableWithoutFeedback>

            <DateTimePickerModal
                date={props.value ? new Date(props.value) : new Date()}
                isVisible={props.isVisible}
                mode="date"
                onConfirm={props.onConfirm}
                onCancel={props.onCancel}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        paddingBottom: '15%'
    },
   
});