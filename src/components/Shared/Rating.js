import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Rating } from 'react-native-elements';

const StarRating = (props) => {

    return (
        <View style={ styles.container }>
            <Rating numberOfLines={1} imageSize={12} readonly startingValue={props.rating} style={styles.rating} />
            <Text style={styles.text}>({props.reviews})</Text>
        </View>
    );
	
}

export default StarRating;

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	star: {
		color: '#FF8C00'
	},
	text: {
		fontSize: 11,
        marginLeft: 5,
        color: '#444',
	},
    rating: {
        alignItems: 'baseline',
    }
});