import React from 'react';
import { TouchableOpacity,Text, StyleSheet } from 'react-native';
export const SubmitButton = (props) => {
  return (
      <TouchableOpacity
        style={ [(!props.disabled? styles.activeButton : styles.inactiveButton), styles.generalButton, {width:props.buttonSize}]}
        onPress={() => {
          if (!props.disabled) props.handleButton();
        }}>
        <Text style={{
            textAlign: 'center',
            fontSize: props.textSize,
            color:'#FFFFFF',
        }}>{props.name}</Text>
  
      </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  activeButton: {
    backgroundColor: '#689CFD',
    borderColor: '#689CFD'
  },
  inactiveButton: { 
    backgroundColor: '#C5C5C5',
    borderColor: '#C5C5C5'
  },
  generalButton: {
    borderRadius:10,
    marginVertical:10,
    padding:15,
    justifyContent: 'center',
    alignItems: 'center',
  }
 
});
export default SubmitButton;