
import React from 'react';
import {Icon, Button, Header, Left, Body, Right, Title } from 'native-base';
import { useNavigation } from '@react-navigation/native';

const HeaderWithBackArrowAndIcon = (props) => {
    const navigation = useNavigation();
    return (
        <Header hasSegment>
            <Left>
                <Button transparent>
                    <Icon name="arrow-back" onPress={ () => {props.navigation.goBack()} } />
                </Button>
            </Left>

            <Body>
                <Title>{props.title}</Title>
            </Body>

            { props.iconFlag === true &&
            <Right>
                <Icon name={props.iconName} onPress={() => {setModalVisible(true);}} />
            </Right>
            }

        </Header>
    )
}

export default HeaderWithBackArrowAndIcon;
