
import React, {Component} from 'react';
import { StyleSheet,View,ActivityIndicator} from 'react-native';


export const LoadingIndicator = () => {
    return (
        <View style={styles.container}>
            <ActivityIndicator size="large" color="#F8169B" />
        </View> 
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: '#ffffff',
        padding: 15, 
    },
   
  });