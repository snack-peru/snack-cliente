import React from 'react';
import { Text, View, Image } from "react-native";
import styles from './Card.style';

const Card = (props) => {

  return (
    <View style={styles.card}>
      <View style={styles.imagecontent}>
        <Image
          source={{ uri: props.categories.image }}
          style={styles.image}
        />
        <Text style={styles.text}>{props.categories.name}</Text>
      </View>
    </View>
  );
}

export default Card;