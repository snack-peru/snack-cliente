import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    list: {
        alignItems: "center", 
        justifyContent: "center"
    },

    card: {
        height: 150,
        width: 150,
        backgroundColor: "#eee", 
        borderRadius: 10
    },

    textview: { 
        width: 150,
        justifyContent: "center",
        alignItems: "center"
    },

    text: {
        fontSize: 20,
        fontWeight: "bold",
        position: "absolute",
        textAlign: "center",
        top: '60%',
        backgroundColor: 'rgba(211, 211, 211,  .7)',
        width: '80%',
        borderRadius: 5
    },

    image: {
        width: 150,
        height: 150,
        borderRadius: 10
    },

     imagecontent: {
        width: 150,
        height: 150,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 10
     }
});