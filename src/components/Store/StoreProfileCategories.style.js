import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        paddingTop: 5,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    storiesContainer: {
        paddingEnd: 10,
        paddingStart: 10,
        alignItems: 'center',
    },
    scrollContent: {
        justifyContent: 'space-evenly',
        width: 150,
        alignItems: 'center',
        alignSelf: 'center'
    },
    scrollImageView: {
        marginHorizontal: 5,
        borderColor: '#e1e1ea',
        borderWidth: 1,
        marginTop: 5,
        marginBottom: 10,
        width: 145,
        height: 180,
        borderRadius: 10,
    },
    thumbnailImageProduct: {
        width: 100, 
        height: 90,
        resizeMode: "contain",
    },
    viewSubtitleCategory:{
        width: '100%',flexDirection:'row',paddingTop:14
    },
    viewNameCategory:{
        width: '70%',flexDirection:'row'
    },
    textNameCategory:{
        fontSize:18,fontWeight:'bold',left:'80%'
    },
    viewSeeAllProductsOfCategory:{
        width: '30%',flexDirection:'row',justifyContent: 'flex-end',right:'2%'
    },
    textSeeAllProductsOfCategory:{
        fontSize:15
    },
    viewProductDetail:{
        paddingLeft:'4%',paddingRight:'4%'
    },
    textPriceProduct:{
        fontSize:14,fontWeight: 'bold'
    },
    textBrandProduct:{
        color:"gray",fontSize:12
    },
    textNameProduct:{
        fontWeight: 'bold',fontSize:12
    },
    textContentProduct:{
        color:"gray",fontSize:12
    }

})