
import React, {useEffect,useState} from 'react';
import { View , Text} from 'react-native';
import { Thumbnail,Container } from 'native-base';
import { TouchableNativeFeedback, ScrollView } from 'react-native-gesture-handler';
import styles from './StoreProfileCategories.style';
import axios from '../../axios';
import { LoadingIndicator } from '../Shared/LoadingIndicator';

 const StoreProfileCategories = props => {

    const [categories,setCategories] = useState(props.categories);
    const [isLoading, setIsLoading] = useState(true);
     

    async function callFunction (){
        await fetchImagesProducts();
    }
    async function fetchImagesProducts() {

        try{
            categories.product.map((product, key) => {
                if (product.image !== null) {
                    let imageProductUrl = axios.defaults.baseURL + "/api/customer/product/image/" + product.image;
                    product.image = imageProductUrl;
                    return product;
                } else {
                    product.image = "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png";
                    return product;
                }}
            );
        }
        catch(error){
            console.log(error);
        }
        setIsLoading(false);

    }

    useEffect(() => {
        console.log("Rendering StoreProfileCategories");
        callFunction();
      }, []);

      if (isLoading) {
        return (
          <Container>
            <LoadingIndicator/>
          </Container>
        );
    
      } else {
        return (
        <View>

        <View style={styles.viewSubtitleCategory}>
            <View style={styles.viewNameCategory}>
                <Text style={styles.textNameCategory}>{props.categories.name}</Text>
            </View>
            <View style={styles.viewSeeAllProductsOfCategory}>
                <Text style={styles.textSeeAllProductsOfCategory}>Ver todo</Text>
            </View>
        </View>

        <View style={styles.container}>
            <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.storiesContainer}
            >

            {categories.product.map((product, index) => 
                    {
                        return(
                            <TouchableNativeFeedback onPress={() => console.log("Producto")}>
                                <View style={styles.scrollContent} >
                                    <View style={styles.scrollImageView}>
                                        <View style={{       alignItems: 'center'}}>
                                            <Thumbnail square large source={{ uri: product.image }} style={styles.thumbnailImageProduct} />
                                        </View>
                                        <View style={styles.viewProductDetail}>
                                            <Text style={styles.textPriceProduct}>S/.{product.price.toFixed(2)}</Text>
                                            <Text style={styles.textBrandProduct}>{product.brand}</Text>
                                            <Text numberOfLines={2} style={styles.textNameProduct}>{product.description} {product.packaging}</Text>
                                            <Text style={styles.textContentProduct}>{product.content}</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableNativeFeedback>
                        );
                    })
            }
            
                        

            </ScrollView>

        </View>
        
        </View>
        

        )
    }
}

export default StoreProfileCategories;