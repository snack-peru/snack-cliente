
import React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import { Badge, Text, Icon } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Rating  from '../Shared/Rating';

export const StoreListItem = (props) => {
    const onPress = () => {
        props.navigation.navigate('AvailableProductStore', { 
            idStore: props.idStore,
            nameStore: props.name,
            availableProducts: props.availableProducts,
            valoration: props.valoration,
            deliveryType: props.deliveryType,
            missingProducts: props.missingProducts
            })
    }
    
    let MissingText = "";
    if(props.missingProducts.length > 0){
        if(props.missingProducts.length === 1)
            MissingText = "Faltante: " + props.missingProducts.length + " producto";
        else 
            MissingText = "Faltantes: " + props.missingProducts.length + " productos";
    }

    return (
        

        <TouchableOpacity style={styles.container} onPress={onPress}>
            <View style={{width: '100%'}}>
              <View style={styles.titleContainer}>
                <Text style={styles.cardTitle}>{props.name}</Text>
              </View> 
              <View style={styles.textContainer}>
                <Rating rating={props.valoration} reviews={45}/>
                <Text style={styles.cardText}> {"Subtotal: " + props.currency} {props.price.toFixed(2)}</Text>
              </View>
              <View style={styles.detailContainer}>
                { props.missingProducts.length > 0
                    ? (
                        <Badge warning>
                            <Text>{MissingText}</Text>
                        </Badge>
                    ) : (                        
                        <Badge success>
                            <Text>Completo</Text>
                        </Badge>
                    )
                }
              </View>
            </View>
        </TouchableOpacity>
    )
}

const screen_height= Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        width:'100%',
        borderRadius: 10,
        marginVertical: '1.5%',
        flexDirection: 'row',
        borderWidth: 0.9,
        borderColor:'#000000',
        height:screen_height/6
    },
    titleContainer:{
        flexDirection: 'row',
        alignItems:'flex-start',
        justifyContent:'flex-start',
        paddingHorizontal: '5%',
        paddingTop: '2%'
    },
    textContainer:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-between',
        
        paddingHorizontal: "5%"
    },
    cardTitle: {
        textAlign: 'left',
        color: '#000000',
        fontSize: 22,
    },
    cardText: {
        textAlign: 'justify',
        color: '#000000',
        fontSize: 14,
    },
    detailContainer: {
        flexDirection: 'row',
        marginHorizontal: '5%',
        margin: '2%',
        justifyContent: 'space-between'
    }
});