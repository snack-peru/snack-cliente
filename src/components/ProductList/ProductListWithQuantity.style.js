import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    leftside: {
        flex: 0.10,
        flexDirection: 'column',
        alignItems: 'center',
    },
    bodyside: {
        flex: 0.30,
        alignItems: 'center'
    },
    rigthside: {
        flex: 0.55, flexDirection: 'column', alignItems: 'flex-start',
    },
    quantityText: {
        fontSize: 15, fontWeight: 'bold',
    },
    unitOfMeasurementText: {
        fontSize: 13
    },
    productNameText: {
        fontSize: 14, fontWeight: 'bold',
    },
    detailContainer: {
        flex: 0.2,
        flexDirection: 'row',
        paddingTop: 5,
    },
    pricePerUnitText: {
        fontSize: 13,
        paddingTop: 1,
        paddingBottom: 1,
        width: 120
    },
    priceText: {
        borderRadius: 7,
        paddingLeft: 7,
        paddingRight: 7,
        paddingTop: 1,
        paddingBottom: 1,
        backgroundColor: '#dbdbdb',
        fontSize: 13,
    }
});