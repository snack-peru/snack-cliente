import React from 'react';
import { Text, View, Image } from "react-native";

import styles from './ProductList.style';

{/*
    Se requiere como input lo siguiente:
    - Objeto "product" (datos del producto), que contenga el "name" e "image"
    - Variable "price" (price del producto)

    La llamada sería de la siguient forma:
    <ProductList product={PRODUCTO} price={PRICE}/>
*/}

const ProductList = (props) => {
        let price = props.price === undefined? "..." : ("S/." + Number(props.price).toFixed(2));
        return (
            <View style={styles.listview}>
                <View style={styles.cardview}>
                    <Image style={styles.imageview} source={{ uri: props.product.image}}/>
                    <View style={styles.nameview}>
                    <Text numberOfLines={2} style={styles.nametext}>{props.product.name}</Text>
                    <View style={styles.brandpriceview}>
                        <View><Text style={styles.brandtext}>{props.product.brand.name}</Text></View>
                        <View style={styles.priceview}><Text style={styles.pricetext}>{price}</Text></View>
                    </View>
                    </View>
                </View>
                
            </View>
        );
}

export default ProductList;