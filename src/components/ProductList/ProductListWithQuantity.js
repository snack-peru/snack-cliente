import React from 'react';
import { View } from 'react-native';
import { Left, Body, Right, ListItem, Text } from 'native-base';
import ImageProduct from '../../components/Cart/ImageProduct';
import styles from './ProductListWithQuantity.style';

{/*
    Se requiere como input lo siguiente:
    - brandName
    - productName
    - unitOfMeasurement
    - quantity
    - unitPrice
    - totalPrice
    - isShowPrices (true/false para mostrar el precio unitario y precio subtotal)

    La llamada sería de la siguient forma:
        <ProductListWithQuantity 
            idProduct = {}
            brandName = {}
            productName = {}
            unitOfMeasurement = {}
            quantity = {}
            unitPrice = {}
            totalPrice = {}
            isShowPrices = {}
        />
*/}

const ProductListWithQuantity = (props) => {
    return (
        <ListItem >
            <Left style={styles.leftside}>
                <View>
                    <Text style={styles.quantityText}>{props.quantity}</Text>
                </View>
                <View>
                    <Text style={styles.unitOfMeasurementText}>{props.unitOfMeasurement}</Text>
                </View>
            </Left>
            <Body style={styles.bodyside}>
                <View>
                    <ImageProduct title={props.idProduct}></ImageProduct>
                </View>
            </Body>
            <Right style={styles.rigthside}>
                <View>{props.brandName !== ""? (<Text style={{ fontSize: 13, textTransform: "uppercase", }}>{props.brandName}</Text>):(<View></View>)}</View>
                <View><Text style={styles.productNameText}>{props.productName}</Text></View>
                    {props.isShowPrices === true? (
                        <View style={styles.detailContainer}>
                            <View><Text style={styles.pricePerUnitText}>{"S/." + props.unitPrice.toFixed(2)} x {props.unitOfMeasurement}</Text></View>
                            <View><Text style={styles.priceText}>{"S/." + (props.totalPrice).toFixed(2)}</Text></View>
                        </View>
                    ): <View></View>}
            </Right>
        </ListItem>
    );
}

export default ProductListWithQuantity;