import { StyleSheet } from 'react-native';

export default StyleSheet.create({

    listview: {
        height: 115,
    },
    cardview: {
        flex: 1,
        flexDirection: 'row',
        margin: 10,
    },
    imageview: {
        width: 80,
        height: 80,
        borderRadius: 10,
        resizeMode: "contain",
    },
    nameview: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
    },
    nametext: {
        fontSize: 17,
        fontWeight: 'bold',
    },
    brandpriceview: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 8,
    },
    brandtext: {
        fontSize: 16,
        textTransform: 'uppercase',
    },
    priceview: {
        position: 'absolute',
        paddingTop: 8,
        right: 0,
    },
    pricetext: {
        fontSize: 15,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor:'#68a0cf',
        borderRadius: 10,
        backgroundColor: '#dbdbdb',
    }

});