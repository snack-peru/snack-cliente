export const isEmptyString = (data) => {
    return !(data.length > 0);
}

export const isInvalidEmail = (email) => {
    let emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return !(emailRegex.test(email))
}

export const isInvalidPhone = (phone) => {
    let phoneRegex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
    return !(phoneRegex.test(phone) && phone.length === 9)
}