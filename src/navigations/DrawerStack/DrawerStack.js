import React from 'react';
import { createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem, } from '@react-navigation/drawer';
import Animated from 'react-native-reanimated';

import BottomTabs from '../BottomTabs/BottomTabs';
import ProfileScreen from '../../screens/Profile/Profile';
import EditProfileScreen from '../../screens/Profile/EditProfile';
import AddressScreen from '../../screens/Address/Address';
import HelpScreen from '../../screens/Profile/Help';
import PaymentMethodScreen from '../../screens/Profile/PaymentMethod';
import ReportScreen from '../../screens/Profile/Report';


import AsyncStorage from '@react-native-community/async-storage'

const Drawer = createDrawerNavigator();

export default function DrawerStack({ navigation }) {

  function CustomDrawerContent({ progress, ...rest }) {
    const translateX = Animated.interpolate(progress, {
      inputRange: [0, 1],
      outputRange: [-100, 0],
    });

    async function logOut() {
      navigation.navigate("Index")

      // Se elimina la información del usuario en AsyncStorage
      await AsyncStorage.removeItem('idCustomer')
      await AsyncStorage.removeItem('idUser')
      await AsyncStorage.removeItem('customerName')
      await AsyncStorage.removeItem('customerEmail')
      await AsyncStorage.removeItem('customerPhone')
      await AsyncStorage.removeItem('customerLocationName')
      await AsyncStorage.removeItem('customerLatitud')
      await AsyncStorage.removeItem('customerLongitude')
    }
  
    return (
      <DrawerContentScrollView {...rest}>
        <Animated.View style={{ transform: [{ translateX }] }}>
          <DrawerItemList {...rest} />
          <DrawerItem label="Cerrar Sesión" onPress={logOut.bind(this)} />
        </Animated.View>
      </DrawerContentScrollView>
    );
  }

  return (
    <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen
        name="Compras"
        component={BottomTabs}
        options={{
          drawerLabel: 'Compras'
        }}
      /> 
      <Drawer.Screen
        name="PaymentMethod"
        component={PaymentMethodScreen}
        options={{ drawerLabel: 'Métodos de pago' }}
      />
      <Drawer.Screen
        name="Address"
        component={AddressScreen}
        options={{ drawerLabel: 'Direcciones' }}
      />
      <Drawer.Screen
        name="Report"
        component={ReportScreen}
        options={{ drawerLabel: 'Reportar un problema' }}
      />
      <Drawer.Screen
        name="Profile"
        component={ProfileScreen}
        options={{ drawerLabel: 'Mi cuenta' }}
      /> 
      <Drawer.Screen
        name="EditProfile"
        component={EditProfileScreen}
        options={{ drawerLabel: 'Editar mi perfil' }}
      />    
      <Drawer.Screen
        name="Help"
        component={HelpScreen}
        options={{ drawerLabel: 'Ayuda' }}
      />
      
      {/* <Drawer.Screen
        name="Profile"
        component={ProfileScreen}
        options={{ drawerLabel: 'Métodos de pago' }}
      />
      <Drawer.Screen
        name="Profile"
        component={ProfileScreen}
        options={{ drawerLabel: 'Direcciones' }}
      />
      <Drawer.Screen
        name="Profile"
        component={ProfileScreen}
        options={{ drawerLabel: 'Reportar un problema' }}
      />
      <Drawer.Screen
        name="Profile"
        component={ProfileScreen}
        options={{ drawerLabel: 'Mi cuenta' }}
      />
      <Drawer.Screen
        name="Profile"
        component={ProfileScreen}
        options={{ drawerLabel: 'Ayuda' }}
      /> */}

      {/* <Drawer.Screen
        name="Notifications"
        component={OrderScreen}
        options={{ drawerLabel: 'Updates' }}
      /> */}

    </Drawer.Navigator>
  );
}
