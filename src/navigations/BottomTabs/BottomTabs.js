import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';

import HomeScreen from '../../screens/Home/Home';
import OrderScreen from '../../screens/Orders/Orders';
import HeartScreen from '../../screens/Heart/Heart';
import CartScreen from '../../screens/Cart/Cart';

import MapScreen from '../../screens/Map/Map';

const Tab = createMaterialBottomTabNavigator();

export default function BottomTabs() {
    return (
        <Tab.Navigator
            initialRouteName="Home"
            activeColor="#f0edf6"
            inactiveColor="#3e2465"
            barStyle={{ backgroundColor: '#694fad' }}
        >
            <Tab.Screen name="Home"
                component={HomeScreen}
                options={{
                    tabBarLabel: 'Inicio',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="home" color={color} size={26} />
                    ),
                }}
            />

            <Tab.Screen name="Mapa"
                component={MapScreen}
                options={{
                    tabBarLabel: 'Mapa',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="map-marker" color={color} size={26} />
                    ),
                }}
            />

            <Tab.Screen name="Favoritos"
                component={HeartScreen}
                options={{
                    tabBarLabel: 'Favoritos',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="heart" color={color} size={26} />
                    ),
                }}
            />

            <Tab.Screen name="Carrito"
                component={CartScreen}
                options={{
                    tabBarLabel: 'Carrito',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="cart" color={color} size={26} />
                    ),
                }}
            />

            <Tab.Screen name="Pedidos"
                component={OrderScreen}
                options={{
                    tabBarLabel: 'Pedidos',
                    tabBarIcon: ({ color }) => (
                        <Entypo name='text-document' color={color} size={26} />
                    ),
                }}
            />

        </Tab.Navigator>
    );
}