import React from 'react';
import { StyleSheet } from 'react-native';
import { Button, Header, Icon, Left, Body, Right, Title, Subtitle } from 'native-base';

const Hamburger = (props) => {
    
    if (props.secondaryHeader === 'gobackOnly') {
        return (
            <Header hasSegment>
                <Left>
                    <Button transparent>
                        <Icon name="arrow-back" onPress={() => { props.navigation.goBack() }} />
                    </Button>
                </Left>
                <Body>
                    <Title>{props.title}</Title>
                </Body>
                <Right />
            </Header>
        );
    }
    else if (props.secondaryHeader === 'search') {
        return (
            <Header hasSegment>
                <Left>
                    <Button transparent>
                        <Icon name="arrow-back" onPress={() => { props.navigation.goBack() }} />
                    </Button>
                </Left>
                <Body>
                    <Title>{props.title}</Title>
                </Body>
                <Right>
                    <Icon name="search" onPress={() => props.navigation.navigate('Search')} />
                </Right>
            </Header>
        );
    } else {
        if (props.subtitle === '') {
            return (
                <Header>
                    <Left>
                        <Button transparent onPress={() => props.navigation.toggleDrawer()} >
                            <Icon name='menu' />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={styles.headerTitle}>{props.title}</Title>
                    </Body>
                    <Right>
                        <Icon name="search" onPress={() => props.navigation.navigate('Search')} />
                    </Right>
                </Header>
            );
        }

        return (
            <Header>
                <Left>
                    <Button transparent onPress={() => props.navigation.toggleDrawer()} >
                        <Icon name='menu' />
                    </Button>
                </Left>
                <Body>
                    <Title>{props.title}</Title>
                    <Subtitle>{props.subtitle}</Subtitle>
                </Body>
            </Header>
        );
    }
}

export default Hamburger;

const styles = StyleSheet.create({
    headerTitle: {
        paddingLeft: 0
    }
});