import React,{useState,useEffect} from 'react';
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native';
import { Button, Header, Icon, Left, Body, Right, Title, Subtitle } from 'native-base';


const HamburgerCart = (props) => {
    let i =0;

    /*useEffect(() => {
        setEditarText('Editar');
        console.log("rendering HamburguerCart");
    
      }, [props.navigation]);*/
    if (props.subtitle === ''){
        return (
            <Header>
            <Left>
                <Button transparent onPress={() => props.navigation.toggleDrawer()} >
                    <Icon name='menu' />
                </Button>
            </Left>
            <Body>
                {props.editar==='Editar'
                    ?<Title>{props.title} ({props.numcesta})</Title>
                    :<Title>{props.numselect} Seleccionados</Title>}                
            </Body>

            <Right>
                <TouchableOpacity>
                    {props.editar==='Editar'
                        ?<Title transparent onPress = {
                                props.onEditBasket.bind(this,'Canasta final')
                            } >{props.editar}</Title>
                        :<Title transparent onPress = {
                                props.onEditBasket.bind(this,'Editando canasta')
                            } >{props.editar}</Title>}
                </TouchableOpacity>
            </Right>
        </Header>
        );
    }

    return (
        <Header>
            <Left>
                <Button transparent onPress={() => props.navigation.toggleDrawer()} >
                    <Icon name='menu' />
                </Button>
            </Left>
            <Body>
                <Title>{props.title}</Title>
                <Subtitle>{props.subtitle}</Subtitle>
            </Body>
            <Right />
        </Header>
    );
}

export default HamburgerCart;